from django.contrib import admin
from checkapp.models import MyUser, Employees, Rotas, ScheduledRotas, Checkin, Checkout, MonthReportNote
from django.contrib.auth.admin import UserAdmin
from ipad.models import *

class CustomUserAdmin(UserAdmin):
    # as an example, this custom user admin orders users by email address
    ordering = ('email',)

# Register your models here.
# admin.site.register(MyUser)


# admin.site.unregister(MyUser)
# admin.site.register(MyUser, CustomUserAdmin)
admin.site.register(MyUser)
admin.site.register(Employees)
admin.site.register(Rotas)
admin.site.register(ScheduledRotas)
admin.site.register(Checkin)
admin.site.register(Checkout)
admin.site.register(MonthReportNote)




