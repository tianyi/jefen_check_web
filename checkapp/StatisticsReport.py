# -*- coding: utf-8 -*-
from datetime import time
# from checkapp.models import ReportColor
from django.conf import settings

'''
    target:

                1001        1002        1003         1004         1005         ....  <- statistics_header
        name    unit_list   unit_list   unti_list    unit_list    unti_list    ....  <- statistics_body
        name    unit_list   unit_list   unti_list    unit_list    unti_list    ....
        name    unit_list   unit_list   unti_list    unit_list    unti_list    ....

        1 build all_exception
        2 put unit with name and exception

    '''

class StatisticsReport():

    def __init__(self, unit_list):
        '''
        source:
                1       2       3      4       5       6       ...
        name    unit    unit    unit   unit    unit    unit    ...   
        name    unit    unit    unit   unit    unit    unit    ...
        name    unit    unit    unit   unit    unit    unit    ...
        '''
        self.__monthReport = unit_list

    '''
        statistics_report
        {'statistics_header': list, 'statistics_body': list}
    '''
    def generateReport(self):

        # return {'statistics_header': [], 'data': []}
        return self.__generateReportImpl()

    def __generateReportImpl(self):
        # build all_exception
        # all_exception = []
        # for un in self.__monthReport:
        #     for uc in un:
        #         # all_exception = [ x["id"]  for x in uc.getStatus() if(x["id"] not in all_exception)]
        #         for x in uc.getStatus():
        #             # import pdb; pdb.set_trace()
        #             if(x["id"] not in all_exception):
        #                 all_exception.append(x["id"])
        # print all_exception

        # build statistics_header
        # statistics_header = [settings.REPORT_STATUS_COLOR[i]  for i in all_exception]

        # for sh in statistics_header:
            # iterate over all the unit to find sh

        # build statistics_body

        # user_list = {}
        '''
            [
                {"name": "", "employee_id": "", "data_list": [{"id":"", "date_list": ["date1", "date2"]}, ]}
                {},
                {},
                ...
            ]
        '''
        statistics_report = []
        # import pdb; pdb.set_trace()

        for un in self.__monthReport:
            name = un[0].getName()
            employee_id = un[0].getEmployeeID()
            exception_temp_dic = {"name": name, "employee_id": employee_id, "data_list": []}
            for uc in un:
                for st in uc.getStatus():
                    # if(type(st) == "str"):
                    #     import pdb; pdb.set_trace()
                    #     print st
                    # print st
                    # import pdb; pdb.set_trace()
                    if(st['id']  == "6000"):
                        # 未排班
                        continue
                    find = False
                    for x in exception_temp_dic["data_list"]:

                        if(x['id'] == st["id"]):
                            x["date_list"].append(uc.getDay())
                            find = True
                            break
                    if(not find):
                        # import pdb; pdb.set_trace()
                        exception_temp_dic["data_list"].append({
                            "id": st["id"],  
                            "name": settings.REPORT_STATUS_COLOR[st["id"]]["name"], 
                            "date_list": [uc.getDay(), ]
                        })
            if(len(exception_temp_dic["data_list"]) > 0):
                statistics_report.append(exception_temp_dic)

        return statistics_report
        
