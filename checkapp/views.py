# -*- coding: utf-8 -*- 

from django.shortcuts import render
# from models import Other2
from models import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from utils import *
from django.http import HttpResponseRedirect, HttpResponse
import simplejson
from datetime import datetime, date
from django.views.decorators.csrf import csrf_exempt
import calendar
from ReportUnit import ReportUnit
from RMReportUnit import RMReportUnit
from StatisticsReport import StatisticsReport
from ScheduleddWorkTime import ScheduleddWorkTime
from django.conf import settings
from datetime import datetime
from django.views.generic import TemplateView, RedirectView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from datetime import time
from django.db.models import Q


# Create your views here.
@login_required
def gotopage(request):
    # 0: 管理员
    # 1：店铺
    # 2：区域经理
    # import pdb; pdb.set_trace()
    if(request.user.user_type == 1):
        return HttpResponseRedirect(reverse('rotas'))
    elif request.user.user_type == 0:
        return HttpResponseRedirect(reverse('report'))
    elif request.user.user_type == 2:
        return HttpResponseRedirect(reverse('regionalmanager_report'))


@login_required
def rotasdelete(request):
    # import pdb; pdb.set_trace()
    rota_id_list = request.POST.getlist('delete')
    Rotas.objects.filter(
        id__in=rota_id_list
        ).delete()
    return HttpResponseRedirect(reverse('rotassetup'))


@login_required
def confirm_modify_button(request):
    # import pdb; pdb.set_trace()
    user_id = int(request.POST.get("user_id"))
    if(request.POST.get('edit_rotas').upper() == "TRUE"):
        edit_rotas = True
    else:
        edit_rotas = False
    mu = MyUser.objects.get(id=user_id)
    mu.edit_rotas = edit_rotas
    try:
        mu.save()
        data = {"status": 1}
    except:
        data = {"status": 0}
    return HttpResponse(simplejson.dumps(data), content_type='application/json')


@login_required
def rotascomfirm(request):
    # user_type = request.user.user_type
    default = {"shop_name": "", "user_id": "", "edit_rotas": ""}
    # import pdb; pdb.set_trace()
    # 店铺
    shop_list = MyUser.objects.filter(user_type=1)
    '''
        {
            "default": {"shop_name": "", "user_id": "", "edit_rotas": ""}, 
            "data": [{"shop_name": "", "user_id": ""}],
        }
    '''
    li = []

    for i in shop_list:
        li.append({"shop_name": i.shop_name, "user_id": i.id, "edit_rotas": i.edit_rotas})
    # import pdb; pdb.set_trace()

    if request.method == "POST":
        post_user_id = int(request.POST.get("user_id"))
        for i in li:
            if(i["user_id"]  == post_user_id):
                default = {"shop_name": i["shop_name"], "user_id": post_user_id, "edit_rotas": i["edit_rotas"]}
                break
    elif request.method == "GET":
        if len(li) > 0:
            default = li[0]
    data = {
        "default": default, 
        "data": li,
    }
    # import pdb; pdb.set_trace()

    if(default["user_id"]):
        ro = Rotas.objects.filter(Q(user_id=default["user_id"]) | Q(is_common=1))
    else:
        ro = []
    # for i in shop_list:

    return render_to_response(
        'checkapp/rotascomfirm.html',
        {"data": data, "rotas_list": ro},
        context_instance=RequestContext(request),
    )

@login_required
def rotassetup(request):
    # import pdb; pdb.set_trace()
    # user_type:
    # 0: 超级管理员
    # 1：店铺
    # 2：普通管理员
    user_type = request.user.user_type
    edit_rotas = request.user.edit_rotas
    # shop_id = request.user.
    # rotas = Rotas.objects.filter(user_id=request.user.id)
    # Poll.objects.get(Q(pub_date=date(2005, 5, 2)) | Q(pub_date=date(2005, 5, 6)))
    rotas = Rotas.objects.filter(Q(user_id=request.user.id) | Q(is_common=1))
    if request.method == 'POST':
        form = RotasForm(request.POST)
        created_time = datetime.now()
        if(form.is_valid()):
            name = form.cleaned_data["name"].strip()
            start_time = form.cleaned_data["start_time"]
            end_time = form.cleaned_data["end_time"]
            rota_type = form.cleaned_data["rota_type"]
            color = form.cleaned_data["color"]
            Rotas(
                user_id=request.user.id,
                rota_type=rota_type,
                name=name,
                start_time=start_time,
                end_time=end_time,
                created_time=created_time,
                color=color,
            ).save()
        else:
            if(form.cleaned_data["rota_type"] == "0"):
                name = form.cleaned_data["name"].strip()
                start_time = "00:00"
                end_time = "00:00"
                rota_type = form.cleaned_data["rota_type"]
                color = form.cleaned_data["color"]
                Rotas(
                    user_id=request.user.id,
                    rota_type=rota_type,
                    name=name,
                    start_time=start_time,
                    end_time=end_time,
                    created_time=created_time,
                    color=color,
                ).save()
                form = RotasForm()
    else:
        form = RotasForm()
    return render_to_response(
        'checkapp/rotassetup.html',
        {'rotas': rotas, 'user_type': user_type, 'edit_rotas': edit_rotas, 'form': form},
        context_instance=RequestContext(request),
    )


# 1 : 出勤
# -1：早退
# -2：迟到
# 2 : 加班
@login_required
def report(request):
    # import pdb; pdb.set_trace()
    # start_time = datetime.now()
    user_type = request.user.user_type
    # user_type:
    # 0: 超级管理员
    # 1：店铺
    # 2：普通管理员
    if(user_type == 1):
        shop_list = [{'id': request.user.shop_id, 'name': request.user.shop_name}]
        if(request.method == "POST"):
            year = int(request.POST.get('year'))
            month = int(request.POST.get('month'))
            shop_id = request.POST.get("shop_id")
        else:
            year = datetime.now().year
            month = datetime.now().month
            shop_id = request.user.shop_id
        
    elif(user_type ==0 or user_type == 2):
        re = MyUser.objects.filter(user_type=1, is_opened=1)
        shop_list = [{'id': i.shop_id, 'name': i.shop_name}  for i in re]
        if(request.method == "POST"):
            year = int(request.POST.get('year'))
            month = int(request.POST.get('month'))
            shop_id = request.POST.get("shop_id")
        else:
            year = datetime.now().year
            month = datetime.now().month
            try:
                shop_id = shop_list[0]['id']
            except:
                shop_id = ''

    year_list = []
    for i in range(settings.REPORT_START_YEAR, settings.REPORT_START_YEAR + settings.DISPLAY_YEAR_NUMBER):
        year_list.append(i)
    year_data = {'year_list': year_list, 'default': year}
    month_data = {'month_list': settings.MONTH_LIST, 'default': month}

    # get shop list

    # 如果店长不在本店排班，那么的话，设置不显示店长
    shop = MyUser.objects.get(shop_id=shop_id)
    # 店长是否在本店排班
    default_shop = {
        'name': shop.shop_name, 
        'id': shop_id, 
        'scheduledRotas': shop.scheduledRotas, 
        'manager_id': shop.manager_id
    }
    shop_data = {
        'shop_list': shop_list,
        'default': default_shop,
    }

    monthRange = getMonthStartEnd(year, month)
    start = monthRange[0]
    end = monthRange[1]
    monthDayCount = monthRange[2]

    # 二维数组
    weekNumList = []
    monthDaylist = []

    for i in range(1, monthDayCount + 1):
        w1 = date(year, month, i).weekday()
        w2 = getWeekdayName(w1)
        weekNumList.append(w2)
        monthDaylist.append(i)
    unit_list = []
    # import pdb; pdb.set_trace()

    if(default_shop["scheduledRotas"]):
        employees = Employees.objects.filter(Q(shop_id=shop_id, status=0) | Q(employee_id=default_shop['manager_id']))
    else:
        employees = Employees.objects.filter(shop_id=shop_id, status=0).exclude(employee_id=default_shop['manager_id'])
    # employees = Employees.objects.filter(shop_id=shop_id, status=0)
    employees_list = [e.employee_id for e in employees]
    u_list = []
    for e in employees:
        u_list = []
        for i in range(0, monthDayCount):
            d = date(year, month, i + 1)
            u = ReportUnit(e.employee_id, e.name, d)
            u_list.append(u)
        unit_list.append(u_list)

    # import pdb; pdb.set_trace()

    sql = "SELECT id, employee_id, check_day, image_path, %s(check_time) check_time \
            FROM checkapp_%s group by check_day, employee_id \
            having check_day >= date('%s') and check_day <= date('%s')"
    sql_checkin = sql % ('min' ,'checkin', start, end)
    sql_checkout = sql % ('max', 'checkout', start, end)

    # print sql_checkout

    checkin = Checkin.objects.raw(sql_checkin)
    checkout = Checkout.objects.raw(sql_checkout)
    # for i in checkin:
    #     print i

    # import pdb; pdb.set_trace()
    start_date = date(year, month, 1)
    end_date = date(year, month, monthDayCount)
    schedule_check = ScheduledRotas.objects.filter(employee_id__in=employees_list, scheduled_day__range=(start_date, end_date)).values(
        "employee_id",
        "rota__name",
        "rota__report_color_id",
        "rota__rota_type",
        "rota__start_time",
        "rota__end_time",
        "weekdayNum",
        "scheduled_day",
    )
    # print 'before setting data:' + str(datetime.now() - start_time)
    checkin_li = []
    # start_time_all = datetime.now()
    for ci in checkin:
        di = {
            "check_time": ci.check_time,
            "image_path": ci.image_path,
            "employee_id": ci.employee_id,
            "check_day": ci.check_day,
        }
        checkin_li.append(di)
    # print 'test checkin_li :' + str(datetime.now() - start_time_all)
    checkout_li = []
    # start_time_all = datetime.now()
    for co in checkout:
        di = {
            "check_time": co.check_time,
            "image_path": co.image_path,
            "employee_id": co.employee_id,
            "check_day": co.check_day,
        }
        checkout_li.append(di)
    # print 'test checkout_li :' + str(datetime.now() - start_time_all)

    # start_time = datetime.now()

    for ul in unit_list:
        # import pdb; pdb.set_trace()
        # start_time_all = datetime.now()
        for un in ul:
            # import pdb; pdb.set_trace()
            # start_time_ci = datetime.now()
            for ci in checkin_li:
                # start_time_ci_one = datetime.now()
                if(ci['employee_id'] == un.employee_id and ci['check_day'] == un.day):
                    # import pdb; pdb.set_trace()
                    un.setCheckinInfo([ci['check_time'], ci['image_path']])
                # print 'set start_time_ci_one, :' + str(datetime.now() - start_time_ci_one)
                   
            # print 'set checkin, :' + str(datetime.now() - start_time_ci)

            # start_time_co = datetime.now()
            for co in checkout_li:
                # import pdb; pdb.set_trace()
                # if(co['employee_id'] == "00000114" and co['check_day'].__str__() == "2015-03-03"):
                #     import pdb; pdb.set_trace()
                if(co['employee_id'] == un.employee_id and co['check_day'] == un.day):
                    un.setCheckoutInfo([co['check_time'], co['image_path']])
            # print 'set check out:' + str(datetime.now() - start_time_co)

            # start_time_sc = datetime.now()
            for sc in schedule_check:
                # import pdb; pdb.set_trace()
                if(sc['employee_id'] == un.employee_id and sc['scheduled_day'] == un.day):
                    un.setScheduleCheck(
                        {
                            'rota_time': [sc['rota__start_time'], sc['rota__end_time']],
                            'rota_type': sc['rota__rota_type'],
                            'rota_name': sc['rota__name'],
                            'report_color_id': sc['rota__report_color_id'],
                        }
                    )
            # print 'set schedule_check: time comsume:' + str(datetime.now() - start_time_sc)
        # print 'set all:' + str(datetime.now() - start_time_all)
    # print 'set checkin, check out, schedule_check: time comsume:' + str(datetime.now() - start_time)
    # start_time = datetime.now()
    # import pdb; pdb.set_trace()
    # print unit_list
    '''
        [{'data': [{'status': 1}, {'status': 2}, ...]}, {}, {}, ...
    '''
    status_list = []
    e_list = []
    for i in unit_list:
        e_list = []
        for j in i:
            # if(j.getDay() == '2015-01-30' and j.getEmployeeID() == '00000284'):
            #     import pdb; pdb.set_trace()
            e_list.append({'status': j.getStatus(), 'eid': j.getEmployeeID(), 'checkday': j.getDay()})
        status_list.append({"name": i[0].getName(), "data": e_list})

    # display note for this month:
    # import pdb; pdb.set_trace()
    try:
        to_user = MyUser.objects.get(shop_id=default_shop["id"])
        report_list = MonthReportNote.objects.filter(report_id=str(year)+str("%02d" % month), user=to_user)
        # note = report_note.note
    except:
        report_list = []
    # initial={'subject': 'Hi there!'}
    month_report_note_form = MonthReportNoteForm()

    # display statistics of all employees
    # report for statistics
    # we can merge this process with report
    '''
        statistics_report
        {'statistics_header': list, 'data': list}
    '''

    statistics_report = StatisticsReport(unit_list).generateReport()

    month_report_id = str(year) + "%02d" % month

    return render_to_response(
        'checkapp/report.html',
        {
            'weekNumList': weekNumList,
            'monthDaylist': monthDaylist,
            'status_list': status_list,
            'year_data': year_data,
            'month_data': month_data,
            'shop_data': shop_data,
            'user_type': user_type,
            'statistics_report': statistics_report,
            'report_list': report_list,
            'month_report_note_form': month_report_note_form,
            'month_report_id': month_report_id,
            'report_note': report_note,
        },
        context_instance=RequestContext(request),
    )

@login_required
def report_print(request):
    # import pdb; pdb.set_trace()
    user_type = request.user.user_type
    # user_type:
    # 0: 超级管理员
    # 1：店铺
    # 2：普通管理员
    if(user_type == 1):
        shop_list = [{'id': request.user.shop_id, 'name': request.user.shop_name}]
        if(request.method == "POST"):
            year = int(request.POST.get('year'))
            month = int(request.POST.get('month'))
            shop_id = request.POST.get("shop_id")
        else:
            year = datetime.now().year
            month = datetime.now().month
            shop_id = request.user.shop_id
        
    elif(user_type ==0 or user_type == 2):
        re = MyUser.objects.filter(user_type=1)
        shop_list = [{'id': i.shop_id, 'name': i.shop_name}  for i in re]
        if(request.method == "POST"):
            year = int(request.POST.get('year'))
            month = int(request.POST.get('month'))
            shop_id = request.POST.get("shop_id")
        else:
            year = datetime.now().year
            month = datetime.now().month
            try:
                shop_id = shop_list[0]['id']
            except:
                shop_id = ''

    year_list = []
    for i in range(settings.REPORT_START_YEAR, settings.REPORT_START_YEAR + 5):
        year_list.append(i)
    year_data = {'year_list': year_list, 'default': year}
    month_data = {'month_list': settings.MONTH_LIST, 'default': month}

    # get shop list
    
    default_shop = {'name': '', 'id': shop_id}
    shop_data = {
        'shop_list': shop_list,
        'default': default_shop,
    }

    monthRange = getMonthStartEnd(year, month)
    start = monthRange[0]
    end = monthRange[1]
    monthDayCount = monthRange[2]

    # 二维数组
    weekNumList = []
    monthDaylist = []

    for i in range(1, monthDayCount + 1):
        w1 = date(year, month, i).weekday()
        w2 = getWeekdayName(w1)
        weekNumList.append(w2)
        monthDaylist.append(i)
    unit_list = []
    # import pdb; pdb.set_trace()
    employees = Employees.objects.filter(shop_id=shop_id, status=0)
    employees_list = [e.employee_id for e in employees]
    u_list = []
    for e in employees:
        u_list = []
        for i in range(0, monthDayCount):
            d = date(year, month, i + 1)
            u = ReportUnit(e.employee_id, e.name, d)
            u_list.append(u)
        unit_list.append(u_list)

    # import pdb; pdb.set_trace()

    sql = "SELECT id, employee_id, check_day, image_path, min(check_time) check_time \
            FROM checkapp_%s group by check_day, employee_id \
            having check_day >= date('%s') and check_day <= date('%s')"
    sql_checkin = sql % ('checkin', start, end)
    sql_checkout = sql % ('checkout', start, end)
    checkin = Checkin.objects.raw(sql_checkin)
    checkout = Checkout.objects.raw(sql_checkout)
    # for i in checkin:
    #     print i

    # import pdb; pdb.set_trace()
    start_date = date(year, month, 1)
    end_date = date(year, month, monthDayCount)
    schedule_check = ScheduledRotas.objects.filter(employee_id__in=employees_list, scheduled_day__range=(start_date, end_date)).values(
        "employee_id",
        "rota__name",
        "rota__report_color_id",
        "rota__rota_type",
        "rota__start_time",
        "rota__end_time",
        "weekdayNum",
        "scheduled_day",
    )

    for ul in unit_list:
        for un in ul:
            for ci in checkin:
                
                if(ci.employee_id == un.employee_id and ci.check_day == un.day):
                    # import pdb; pdb.set_trace()
                    un.setCheckinInfo([ci.check_time, ci.image_path])
                    # print '---------------------'
                    # print un.getName()
                    # print un.getEmployeeID()
                    # print un.getDay()
                    # print un.getCheckinInfo()
                    # print un.getCheckoutInfo()
                    # print '---------------------'
            for co in checkout:
                # import pdb; pdb.set_trace()
                if(co.employee_id == un.employee_id and co.check_day == un.day):
                    un.setCheckoutInfo([co.check_time, co.image_path])
            for sc in schedule_check:
                # import pdb; pdb.set_trace()
                if(sc['employee_id'] == un.employee_id and sc['scheduled_day'] == un.day):
                    un.setScheduleCheck(
                        {
                            'rota_time': [sc['rota__start_time'], sc['rota__end_time']],
                            'rota_type': sc['rota__rota_type'],
                            'rota_name': sc['rota__name'],
                            'report_color_id': sc['rota__report_color_id'],
                        }
                    )

    # import pdb; pdb.set_trace()
    # print unit_list
    '''
        [{'data': [{'status': 1}, {'status': 2}, ...]}, {}, {}, ...
    '''
    status_list = []
    e_list = []
    for i in unit_list:
        e_list = []
        for j in i:
            # if(j.getDay() == '2015-01-30' and j.getEmployeeID() == '00000284'):
            #     import pdb; pdb.set_trace()
            e_list.append({'status': j.getStatus(), 'eid': j.getEmployeeID(), 'checkday': j.getDay()})
        status_list.append({"name": i[0].getName(), "data": e_list})

    # display note for this month:
    # import pdb; pdb.set_trace()
    try:
        to_user = MyUser.objects.get(shop_id=default_shop["id"])
        report_note = MonthReportNote.objects.get(report_id=str(year)+str("%02d" % month), user=to_user)
        note = report_note.note
    except:
        note = ""
    # initial={'subject': 'Hi there!'}
    report_note = {'note': note}
    month_report_note_form = MonthReportNoteForm(initial=report_note)

    # display statistics of all employees
    # report for statistics
    # we can merge this process with report
    '''
        statistics_report
        {'statistics_header': list, 'data': list}
    '''
    statistics_report = StatisticsReport(unit_list).generateReport()
    # print statistics_report
    # import pdb; pdb.set_trace()
    month_report_id = str(year) + "%02d" % month


    return render_to_response(
        'checkapp/report_print.html',
        {
            'weekNumList': weekNumList,
            'monthDaylist': monthDaylist,
            'status_list': status_list,
            'year_data': year_data,
            'month_data': month_data,
            'shop_data': shop_data,
            'user_type': user_type,
            'statistics_report': statistics_report,
            'month_report_note_form': month_report_note_form,
            'month_report_id': month_report_id,
            'report_note': report_note,
        },
        context_instance=RequestContext(request),
    )


# 用来接收client端的HTTP POST check in数据
@csrf_exempt
def uploadImageInfoCheckin(request):
    # import pdb; pdb.set_trace()
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            image_path = form.cleaned_data['image_path']
            employee = request.POST.get('employee_id')
            checkin_time = request.POST.get('check_time')
            check_day = request.POST.get('check_day')
            try:
                shop_id = request.POST.get('shop_id')
            except:
                shop_id = settings.CLIENT_VERSION
            if(not shop_id):
                shop_id = settings.CLIENT_VERSION
            Checkin.objects.create(
                employee_id=employee,
                check_time=checkin_time,
                check_day=check_day,
                image_path=image_path,
                shop_id=shop_id,
            )
            data = {"status": 1}
        else:
            # something wrong
            data = {"status": -1}
    else:
        data = {"status": 0}
    return HttpResponse(simplejson.dumps(data), content_type='application/json')

@login_required
def report_note(request):
    if(request.method == "POST"):
        # import pdb; pdb.set_trace()
        # month_report_note_form = MonthReportNoteForm(request.POST)

        obj = MonthReportNote(
            user=request.user,
            report_id = request.POST.get('report_id'),
            note = request.POST.get('note'),
            created_time = datetime.now(),
        )
        obj.save()

        # x = month_report_note_form.save(commit=False)
        # x.user = request.user
        # x.created_time = datetime.now()
        # x.report_id = request.POST.get('report_id')
        # x.save()

    return HttpResponseRedirect(reverse('report'))

# @login_required
# def print_report(request):


# 用来接收client端的HTTP POST check in数据
@csrf_exempt
def uploadImageInfoCheckout(request):
    # import pdb; pdb.set_trace()
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            image_path = form.cleaned_data['image_path']
            employee = request.POST.get('employee_id')
            checkin_time = request.POST.get('check_time')
            check_day = request.POST.get('check_day')
            try:
                shop_id = request.POST.get('shop_id')
            except:
                shop_id = settings.CLIENT_VERSION
            if(not shop_id):
                shop_id = settings.CLIENT_VERSION
            Checkout.objects.create(
                employee_id=employee,
                check_time=checkin_time,
                check_day=check_day,
                image_path=image_path,
                shop_id=shop_id,
            )
            data = {"status": 1}
        else:
            # something wrong
            data = {"status": -1}
    else:
        # form = ImageForm()
        data = {"status": 0}
    # return render_to_response(
    #     'checkapp/imagetest.html',
    #     {'form': form},
    #     context_instance=RequestContext(request),
    # )

    
    return HttpResponse(simplejson.dumps(data), content_type='application/json')
    

@login_required
def rotas(request):

    # import pdb; pdb.set_trace()
    user_type = request.user.user_type
    try:
        date1 = request.GET.get("date")
    except:
        date1 = None
    today = datetime.today()
    year, month, day = date1.split("-") if(date1) else (today.year, today.month, today.day)
    year = int(year)
    month = int(month)
    day = int(day)
    weekdays = getWeekDays(year, month, day)
    date_rage = {"start_date": weekdays[0]["date"], "end_date": weekdays[6]["date"]}
    start_date = string_toDatetime(weekdays[0]["date"])
    end_date = string_toDatetime(weekdays[6]["date"])
    rotas = Rotas.objects.filter(Q(user_id=request.user.id) | Q(is_common=1)).order_by("-id")

    # 如果店长不在本店排班，那么的话，设置不显示店长
    shop_manager = request.user.manager_id
    # 店长是否在本店排班
    scheduledRotas = request.user.scheduledRotas
    if(scheduledRotas):
        employees = Employees.objects.filter(Q(shop_id=request.user.shop_id, status=0) | Q(employee_id=shop_manager))
    else:
        employees = Employees.objects.filter(shop_id=request.user.shop_id, status=0).exclude(employee_id=shop_manager)
    # import pdb; pdb.set_trace()
    scheduledRotas = ScheduledRotas.objects.filter(scheduled_day__range=(start_date, end_date)).values(
        "employee__employee_id",
        "rota__name",
        "weekdayNum",
        'scheduled_day',
        "rota__color"
    )
    '''
        {'name': name, 'cell_data': [{'employee_id': '', 'weekdayNumofYear': 50, weekNum: '',rota__name: '' }, ]}
        {'name': name, 'cell_data': [{'employee_id': '', 'weekdayNumofYear': 50, weekNum: '',rota__name: '' , scheduled_day: ''}, ]}
    '''
    cell_list = []
    cell_data_list = []
    # weekdays = getWeekdays(2014, weekdayNumofYear)
    '''
        example: test.json  
        {"employee_id": "id", "rota__name":"rota__name", "weekNum": 0, "scheduled_day": "2014-12-29"}, 
    '''
    found = 0
    for e in employees:
        cell_data_list = []

        for weekDayNum in range(0, 7):
            # import pdb; pdb.set_trace()
            day_this = string_toDatetime(weekdays[weekDayNum]['date'])
            if(day_this <= datetime.now()):
                editable = 0
            else:
                editable = 1
            for sr in scheduledRotas:
                # editable = 0
                # import pdb; pdb.set_trace()
                if(sr['employee__employee_id'] == e.employee_id and sr['weekdayNum'] == weekDayNum):
                    cell_data_list.append({
                        'employee_id': e.employee_id, 
                        'weekNum': weekDayNum, 
                        'rota__name': sr['rota__name'],
                        'scheduled_day': sr['scheduled_day'].isoformat(),
                        'rota_color': sr['rota__color'],
                        'editable': editable,
                    })
                    found = 1
                    break
                # not schedule day
            if(not found):
                cell_data_list.append(
                    {
                        'employee_id': e.employee_id, 
                        'weekNum': weekDayNum, 
                        'rota__name': '',
                        'scheduled_day': weekdays[weekDayNum]["date"],
                        'editable': editable,
                    }
                )
            else:
                found = 0
        cell_list.append(
            {
                "name": e.name,
                "data": cell_data_list
            }
        )
    # print cell_list
    # import pdb; pdb.set_trace()
    popUphtml = ''
    # name, eid, rotaid, scheduledate, weekdaynum
    # html = '<button onclick=myFunction(%s,%s,%s,%s,%s) type=button>%s</button>'
    rota_button = '<button class=\\"btn btn-default\\" style=\\"background-color:%s\\" onclick=\'myFunction(\\"%s\\",\\"EID\\",%s,\\"SCHEDULEDATE\\",\\"WEEKDAYNUM\\")\' type=button>%s</button>'
    clear_button = '<button class=\\"btn btn-default\\" onclick=\'myFunction(\\"\\",\\"EID\\",-1,\\"SCHEDULEDATE\\",\\"WEEKDAYNUM\\")\' type=button>'+u'清除'+'</button>'
    for r in rotas:
        # print r
        
        popUphtml += rota_button % (r.color, r.name, r.id, r.name)

    # add clear button
    popUphtml += clear_button
    popUphtml = '<div id=PopUp>' + popUphtml + '</div>'

    # print popUphtml
    return render_to_response(
        'checkapp/rotas.html',
        {
            'popUphtml': popUphtml, 
            'cell_list': cell_list, 
            'weekdays': weekdays,
            'date_rage': date_rage,
            'user_type': user_type,
        },
        context_instance=RequestContext(request),
    )
    

@login_required
def employeeinfo(request):
    # import pdb; pdb.set_trace()
    user_type = request.user.user_type

    employees = Employees.objects.filter(shop_id=request.user.shop_id, status=0)
    employees_list = [{'employee_id': em.employee_id, 'name': em.name, 'shop_id': em.shop_id}  for em in employees]

    return render_to_response(
        'checkapp/employeeinfo.html',
        {
            'employees': employees_list,
            'user_type': user_type,
        },
        context_instance=RequestContext(request),
    )

# @login_required
def test(request):
    if(request.method  == "PSOT"):
        name = request.POST.get("name")
        em = Employees.objects.filter(name__contains=name)
    else:
        em = None
    
    return render_to_response(
        'checkapp/test.html',
        {
            'em': em,
        },
        context_instance=RequestContext(request),
    )


# ajax: save data
@login_required
def setuprota(request):
    # import pdb; pdb.set_trace()
    # print request.POST
    eid = request.POST.get("eid")
    rotaid = request.POST.get("rotaid")
    scheduledate = request.POST.get("scheduledate")
    weekdaynum = request.POST.get("weekdaynum")
    # print rotaid
    # print cell_id
    scheduled_day = string_toDatetime(scheduledate)
    if(rotaid == '-1'):
        ScheduledRotas.objects.filter(
            employee_id=eid, 
            scheduled_day=scheduled_day).delete()
        clear = 1
        color = ""
    else:
        obj, created = ScheduledRotas.objects.get_or_create(
            employee_id=eid,
            scheduled_day=scheduled_day,
            defaults={
                'rota_id': rotaid, 
                'scheduled_day': scheduled_day,
                'weekdayNum': weekdaynum,
            }
        )
        if(not created):
            obj.rota_id = rotaid
        obj.save()
        rota = Rotas.objects.get(id=rotaid)
        color = rota.color
        clear = 0
    
    data = {"status": 200, "color": color, "clear": clear}

    return HttpResponse(simplejson.dumps(data), content_type='application/json')

# @login_required
# def form_test(request):
#     """
#     ----------------------------------------------
#     Function:    form测试
#     DateTime:    2013/9/9
#     ----------------------------------------------
#     """
#     other = Other2.objects.all()
#     if request.method == 'POST':
#         form = ContactForm(request.POST)  # 绑定post数据
#         if form.is_valid():  # 如果通过验证
#             # 数据清理
#             name = form.cleaned_data['name']
#             age = form.cleaned_data['age']
#             email = form.cleaned_data['email']
#             Other2.objects.create(name=name, age=age, email=email).save()
#             return render_to_response('checkapp/form.html', {'form': form, 'other': other})
#     else:
#         # 如果表单未被提交，则一个未绑定的表单实例被创建
#         form = ContactForm(initial={'name': 'Hi,here', 'email': '@'})    # 未绑定的表单
#     return render_to_response(
#         'checkapp/form.html',
#         {'form':form,'other':other}, 
#         context_instance=RequestContext(request))

#ajax: load pic
@login_required
def loadPic(request):
    # import pdb; pdb.set_trace()
    eid = request.POST.get('eid')
    checkday = request.POST.get('checkday')
    # Checkin.objects.get()
    sql = "SELECT id, employee_id, check_day, image_path, min(check_time) check_time, shop_id \
            FROM checkapp_%s group by check_day, employee_id \
            having check_day = date('%s') and employee_id = \"%s\" "
    # sql = "SELECT c.id, c.employee_id, c.check_day, c.image_path, min(c.check_time) check_time, u.shop_name \
    #        FROM checkapp_%s c, checkapp_myuser u \
    #        where c.shop_id=u.username \
    #        group by c.check_day, c.employee_id \
    #        having c.check_day = date('%s') and c.employee_id = '%s'"

    sql_checkin = sql % ('checkin', checkday, eid)
    sql_checkout = sql % ('checkout', checkday, eid)
    # only get one row data
    checkin = Checkin.objects.raw(sql_checkin)
    checkout = Checkout.objects.raw(sql_checkout)


    # data = {}
    data = {
        'checkin_image_path': '', 
        'checkout_image_path': '', 
        'checkin_time': '',
        'checkout_time': '',
        'shop_name_in': '',
        'shop_name_out': '',
    }

    for i in checkin:
        try:
            shop_name_in = MyUser.objects.get(username=i.shop_id).shop_name
        except:
            shop_name_in = ""
        data.update({'checkin_image_path': i.image_path.url, 'checkin_time': i.check_time, 'shop_name_in': shop_name_in})

    for i in checkout:
        try:
            shop_name_out = MyUser.objects.get(username=i.shop_id).shop_name
        except:
            shop_name_out = ""
        data.update({'checkout_image_path': i.image_path.url, 'checkout_time': i.check_time, 'shop_name_out': shop_name_out})
    # print data
    return HttpResponse(simplejson.dumps(data), content_type='application/json')

# 0: fail 1: suc
@csrf_exempt
def loginValidation(request):
    # import pdb; pdb.set_trace()
    username = request.POST.get("username")
    password = request.POST.get("password")
    data = None
    
    try:
        u = MyUser.objects.get(username__iexact=username, user_type=1)
        if(u.check_password(password)):
            data = {
                "status": 1, 
                "msg": "successful", 
                "username": u.username, 
                "usertype": u.user_type,
                "shop_name": u.shop_name,
                "shop_status": u.shop_status,
                "shop_id": u.shop_id,
            }
        else:
            data = {
                "status": -1, 
                "msg": "password is wrong",
            }
    except:
        data = {
                "status": -1, 
                "msg": "fail",
        }
    return HttpResponse(simplejson.dumps(data), content_type='application/json')

@csrf_exempt
def getEmployeesInfo(request):
    # import pdb; pdb.set_trace()
    if(request.method == "POST"):
        shop_id = request.POST.get("shop_id")
    else:
        shop_id = request.GET.get("shop_id")
    shop = MyUser.objects.get(shop_id=shop_id)
    li = []
    try:
        # em = Employees.objects.filter(shop_id=shop_id)
        if(shop.scheduledRotas):
            em = Employees.objects.filter(Q(shop_id=shop_id, status=0) | Q(employee_id=shop.manager_id))
        else:
            em = Employees.objects.filter(shop_id=shop_id, status=0)
        for i in em:
            # print i.name
            if(not i.status):

                li.append(
                    {
                        "name": i.name,
                        "status": i.status,
                        "employee_id": i.employee_id,
                        "shop_id": i.shop_id,
                        # 0: 普通员工， 1 区域经理
                        "employee_type": 0,
                    }
                )

        rm = Employees.objects.filter(shop_id=settings.REGIONAL_MANAGER_SHOPNAME)
        for i in rm:
            if(not i.status):
                li.append(
                    {
                        "name": i.name,
                        "status": i.status,
                        "employee_id": i.employee_id,
                        "shop_id": i.shop_id,
                        # 0: 普通员工，1 区域经理
                        "employee_type": 1,
                    }
                )

        data = {
                "status": 1,
                "msg": "successful",
                "data": li,
        }
    except:    
        data = {
                "status": -1, 
                "msg": "fail",
        }
    return HttpResponse(simplejson.dumps(data), content_type='application/json')


@login_required
def schedule_work_time(request):
    # user_type:
    # 0: 超级管理员
    # 1：店铺
    # 2：普通管理员
    user_type = request.user.user_type
    if(user_type == 1):
        shop_list = [{'id': request.user.shop_id, 'name': request.user.shop_name}]
        if(request.method == "POST"):
            year = int(request.POST.get('year'))
            month = int(request.POST.get('month'))
            shop_id = request.POST.get("shop_id")
        else:
            year = datetime.now().year
            month = datetime.now().month
            shop_id = request.user.shop_id
        
    elif(user_type ==0 or user_type == 2):
        re = MyUser.objects.filter(user_type=1)
        shop_list = [{'id': i.shop_id, 'name': i.shop_name}  for i in re]
        if(request.method == "POST"):
            year = int(request.POST.get('year'))
            month = int(request.POST.get('month'))
            shop_id = request.POST.get("shop_id")
        else:
            year = datetime.now().year
            month = datetime.now().month
            try:
                shop_id = shop_list[0]['id']
            except:
                shop_id = ''

    year_list = []
    for i in range(settings.REPORT_START_YEAR, settings.REPORT_START_YEAR + settings.DISPLAY_YEAR_NUMBER):
        year_list.append(i)
    year_data = {'year_list': year_list, 'default': year}
    month_data = {'month_list': settings.MONTH_LIST, 'default': month}

    default_shop = {'name': '', 'id': shop_id}
    shop_data = {
        'shop_list': shop_list,
        'default': default_shop,
    }

    report = ScheduleddWorkTime(
            shop_id=shop_data['default']['id'], 
            year=year_data['default'], 
            month=month_data['default']
    ).generateReport()
    # print report

    return render_to_response(
        'checkapp/schedule_work_time.html',
        {
            "year_data": year_data,
            "month_data": month_data,
            "shop_data": shop_data,
            "report": report,
            "user_type": user_type,
        },
        context_instance=RequestContext(request),
    )

@login_required
def regionalmanager(request):

    rm = Employees.objects.filter(shop_id=settings.REGIONAL_MANAGER_SHOPNAME)
    # import pdb; pdb.set_trace()
    if(request.method == "POST"):
        employees_form = EmployeesForm(request.POST)
        
        mo = employees_form.save(commit=False)

        mo.shop_id = settings.REGIONAL_MANAGER_SHOPNAME
        mo.status = 0

        mo.save()
    else:
        employees_form = EmployeesForm(initial={"employee_id":"RM00001"})

    return render_to_response(
        'checkapp/regional_manager.html',
        {
            "regional_manager": rm,
            "employees_form": employees_form,
        },
        context_instance=RequestContext(request),
    )

@login_required
def regionalmanager_report(request):
    # import pdb; pdb.set_trace()
    # start_time = datetime.now()
    user_type = request.user.user_type
 
    if(request.method == "POST"):
        year = int(request.POST.get('year'))
        month = int(request.POST.get('month'))
        # shop_id = request.POST.get("shop_id")
    else:
        year = datetime.now().year
        month = datetime.now().month

    year_list = []
    for i in range(settings.REPORT_START_YEAR, settings.REPORT_START_YEAR + settings.DISPLAY_YEAR_NUMBER):
        year_list.append(i)
    year_data = {'year_list': year_list, 'default': year}
    month_data = {'month_list': settings.MONTH_LIST, 'default': month}

    monthRange = getMonthStartEnd(year, month)
    start = monthRange[0]
    end = monthRange[1]
    monthDayCount = monthRange[2]

    # 二维数组
    weekNumList = []
    monthDaylist = []

    for i in range(1, monthDayCount + 1):
        w1 = date(year, month, i).weekday()
        w2 = getWeekdayName(w1)
        weekNumList.append(w2)
        monthDaylist.append(i)
    unit_list = []
    employees = Employees.objects.filter(shop_id=settings.REGIONAL_MANAGER_SHOPNAME)
    employees_list = [e.employee_id for e in employees]
    u_list = []
    for e in employees:
        u_list = []
        for i in range(0, monthDayCount):
            d = date(year, month, i + 1)
            u = RMReportUnit(e.employee_id, e.name, d)
            u_list.append(u)
        unit_list.append(u_list)

    sql = "SELECT c.id, c.employee_id, c.check_day, c.image_path, %s(c.check_time) check_time \
           FROM checkapp_%s c, checkapp_employees e \
           where e.employee_id=c.employee_id \
                 and e.shop_id='%s' \
                 group by c.check_day, c.employee_id \
                 having c.check_day >= date('%s') and c.check_day <= date('%s')"
    sql_checkin = sql % ('min', 'checkin', settings.REGIONAL_MANAGER_SHOPNAME, start, end)
    # print sql_checkin
    sql_checkout = sql % ('max', 'checkout', settings.REGIONAL_MANAGER_SHOPNAME, start, end)
    # print sql_checkout
    checkin = Checkin.objects.raw(sql_checkin)
    checkout = Checkout.objects.raw(sql_checkout)

    start_date = date(year, month, 1)
    end_date = date(year, month, monthDayCount)
    
    checkin_li = []
    for ci in checkin:
        di = {
            "check_time": ci.check_time,
            "image_path": ci.image_path,
            "employee_id": ci.employee_id,
            "check_day": ci.check_day,
        }
        checkin_li.append(di)

    # print checkin_li

    checkout_li = []
    for co in checkout:
        di = {
            "check_time": co.check_time,
            "image_path": co.image_path,
            "employee_id": co.employee_id,
            "check_day": co.check_day,
        }
        checkout_li.append(di)

    # print checkout_li


    for ul in unit_list:
        for un in ul:
            for ci in checkin_li:
                if(ci['employee_id'] == un.employee_id and ci['check_day'] == un.day):
                    un.set_checkin([ci['check_time'], ci['image_path']])
            for co in checkout_li:
                if(co['employee_id'] == un.employee_id and co['check_day'] == un.day):
                    un.set_checkout([co['check_time'], co['image_path']])
    '''
        [{
        'data': {
                    'checkin_time': '20:15', 
                    'checkout_time': '21:30', 
                    'check_day': '2015-03-01', 
                    'eid': '00000016',
            }
        }]
    '''
    status_list = []
    e_list = []
    for i in unit_list:
        e_list = []
        for j in i:
            # e_list.append({'status': j.getStatus(), 'eid': j.getEmployeeID(), 'checkday': j.getDay()})
            e_list.append({
                'checkin_time': j.get_checkin()[0], 
                'checkout_time': j.get_checkout()[0], 
                'check_day': j.get_check_day(), 
                'eid': j.get_eid})
        status_list.append({"name": i[0].get_name(), "data": e_list})

    return render_to_response(
        'checkapp/regionalmanager_report.html',
        {
            'weekNumList': weekNumList,
            'monthDaylist': monthDaylist,
            'status_list': status_list,
            'year_data': year_data,
            'month_data': month_data,
            'user_type': user_type,
        },
        context_instance=RequestContext(request),
    )

def help(request):
    return render_to_response(
        'checkapp/help.html',
        {
        },
        context_instance=RequestContext(request),
    )

@login_required
def feedback(request):
    # import pdb; pdb.set_trace()
    message = None
    if(request.method == "POST"):
        form = FeedBackForm(request.POST)
        if(form.is_valid()):
            mo = form.save(commit=False)
            mo.user = request.user
            mo.save()
            form = FeedBackForm()
            message = u"保存成功，谢谢！"
    else:
        form = FeedBackForm()

    return render_to_response(
        'checkapp/feedback.html',
        {
            "form": form,
            "message": message,
        },
        context_instance=RequestContext(request),
    )

@login_required
def delete_feedback(request):
    # import pdb; pdb.set_trace()
    feed_id = request.POST.get("input_value")
    FeedBack.objects.filter(id=feed_id).delete()
    return HttpResponseRedirect(reverse('list_feedback'))

# from django.views.generic.edit import DeleteView
# from django.core.urlresolvers import reverse_lazy
# from myapp.models import Author