# -*- coding: utf-8 -*-

from django import forms
from django.http import HttpResponse
from django.views.generic import View
from django.shortcuts import render

from checkapp.models import *

# sql = "select  \
#             ci.id,  \
#             ci.employee_id,  \
#             ci.check_day,  \
#             min(ci.check_time) checkin_time,  \
#             max(co.check_time) checkout_time  \
#         from  \
#             checkapp_checkin ci,  \
#             checkapp_checkout co \
#         where  \
#             ci.employee_id='%s' \
#             and ci.employee_id=co.employee_id \
#             and ci.check_day=co.check_day \
#             group by ci.check_day"

sql_out = "select  \
            c.id,  \
            c.employee_id,  \
            c.check_day,  \
            max(c.check_time) checkout_time \
        from checkapp_checkout c \
        where c.employee_id='%s' \
        group by c.check_day"

sql_in = "select  \
            c.id,  \
            c.employee_id,  \
            c.check_day,  \
            max(c.check_time) checkin_time \
        from checkapp_checkin c \
        where c.employee_id='%s' \
        group by c.check_day"



class MessageForm(forms.Form):

    message = forms.CharField(label=u'输入员工姓名')


class GetSingleUserCheck(View):

    form_class = MessageForm
    template_name = 'checkapp/get_single_user_check.html'

    def get(self, request):
        # <view logic>
        # import pdb; pdb.set_trace()
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        # import pdb; pdb.set_trace()
        form = self.form_class(request.POST)
        object_list = []
        
        if form.is_valid():
            em_list = Employees.objects.filter(name=form.cleaned_data["message"])
            # if em_list:

            #     for em in em_list:
            #         sub_object_list = []
            #         sql_check = sql % (em.employee_id)
            #         check = Checkin.objects.raw(sql_check)
            #         for ck in check:
            #             sub_object_list.append(ck)
            #         object_list.append(sub_object_list)

            if em_list:
                for em in em_list:
                    sub_object_list = []

                    sql = sql_in % (em.employee_id)
                    print sql
                    checkin = Checkin.objects.raw(sql)
                    for ci in checkin:
                        cm = CheckModel()
                        cm.employee_id   = ci.employee_id
                        cm.check_day     = ci.check_day
                        cm.checkin_time  = ci.checkin_time
                        sub_object_list.append(cm)

                    sql = sql_out % (em.employee_id)
                    print sql
                    checkout = Checkin.objects.raw(sql)
                    for co in checkout:
                        found = False
                        for ff in sub_object_list:
                            if(ff.employee_id==co.employee_id and ff.check_day==co.check_day):
                                ff.checkout_time = co.checkout_time
                                found = True
                        if not found:
                            cm = CheckModel()
                            cm.employee_id   = co.employee_id
                            cm.check_day     = co.check_day
                            cm.checkout_time  = co.checkout_time
                            sub_object_list.append(cm)
                    object_list.append(sub_object_list)
            # import pdb; pdb.set_trace()
            # for obj in len(object_list:
            #     sorted(obj, key=lambda xxx: xxx.check_day)
            #     # sorted(xxx[0], key=lambda obj: obj.check_day)
            object_list_1 = []
            for ii in range(len(object_list)):
                object_list_1.append(sorted(object_list[ii], key=lambda xxx: xxx.check_day))

        return render(request, self.template_name, {'form': form, 'object_list': object_list_1})


        # select c.id, c.employee_id, c.check_day, min(c.check_time) checkin_time
        # from checkapp_checkin c
        # where c.employee_id='00000114' and c.check_day='2015-02-06'
        # group by c.check_day


        # select c.id, c.employee_id, c.check_day, max(c.check_time) checkout_time
        # from checkapp_checkout c
        # where c.employee_id='00000114' and c.check_day='2015-02-06'
        # group by c.check_day


        # select ci.id, ci.employee_id, ci.check_day, min(ci.check_time) checkin_time, max(co.check_time) checkout_time 
        # from checkapp_checkin ci, checkapp_checkout co
        # where ci.employee_id='00000114'
        #     and ci.employee_id=co.employee_id
        #     and ci.check_day='2015-02-06'
        #     and ci.check_day=co.check_day
        # group by ci.check_day


        
        # select c.id, c.employee_id, c.check_day, c.check_time 
        # from checkapp_checkin c
        # where c.employee_id='00000114' and c.check_day='2015-02-06'


        # select c.id, c.employee_id, c.check_day, c.check_time 
        # from checkapp_checkout c
        # where c.employee_id='00000114' and c.check_day='2015-02-06'


class CheckModel():

    employee_id = ""
    check_day   = ""
    checkin_time = ""
    checkout_time = ""

    def __init__(self):
        pass

    def __str__(self):
        return self.check_day.__str__()

    def __repr__(self):
        return repr((self.check_day))