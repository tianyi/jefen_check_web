# -*- coding: utf-8 -*- 
from datetime import datetime, date, timedelta
import time
import calendar


#把datetime转成字符串  
def datetime_toString(dt):
    return dt.strftime("%Y-%m-%d")  
  

#把字符串转成datetime  
def string_toDatetime(string):  
    return datetime.strptime(string, "%Y-%m-%d")  
  

#把字符串转成时间戳形式  
def string_toTimestamp(strTime):  
    return time.mktime(string_toDatetime(strTime).timetuple())  
  

#把时间戳转成字符串形式  
def timestamp_toString(stamp):  
    return time.strftime("%Y-%m-%d-%H", tiem.localtime(stamp))  
  

#把datetime类型转外时间戳形式  
def datetime_toTimestamp(dateTim):  
    return time.mktime(dateTim.timetuple())


def getDateTimeNow():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')


# def getWeekdays(year, week):
#     january_first = date(year, 1, 1)
#     monday_date = january_first + timedelta(days=week * 7 - january_first.weekday())

#     # monday, tuesday, .. sunday
#     # return [(monday_date + timedelta(days=d)).day for d in range(7)]
#     return [(monday_date + timedelta(days=d)).isoformat() for d in range(7)]


# def getIsocalendar(year, month, day):

#     return datetime(year, month, day).isocalendar()


def getMonthStartEnd(year, month):
    monthRange = calendar.monthrange(int(year), int(month))

    m = "%02d" % month
    start = str(year) + '-' + str(m) + '-01'
    end = str(year) + '-' + str(m) + '-' + str(monthRange[1])
    return (start, end, monthRange[1])


def getMonthDayList(year, month):
    monthRange = calendar.monthrange(int(year), int(month))
    monday_date = date(year, month, 1)
    return [(monday_date + timedelta(days=d)) for d in range(monthRange[1])]


def getWeekDays(year, month, day):
    c_day = date(year, month, day)
    weekday = c_day.weekday()
    first_day_week = date(year, month, day) - timedelta(days=weekday)
    return [{"date": (first_day_week + timedelta(days=d)).isoformat(), "weekName": getWeekdayName(d)} for d in range(7)]


def getWeekdayName(d):
    if(d == 0):
        return "一"
    if(d == 1):
        return "二"
    if(d == 2):
        return "三"
    if(d == 3):
        return "四"
    if(d == 4):
        return "五"
    if(d == 5):
        return "六"
    if(d == 6):
        return "日"
    return "出错"

def timeTimedelta(t1, t2, re_type="s"):
        # import pdb; pdb.set_trace()
        h1 = t1.hour
        m1 = t1.minute
        s1 = t1.second

        h2 = t2.hour
        m2 = t2.minute
        s2 = t2.second
        all_second_t1 = h1*3600 + m1*60 + s1
        all_second_t2 = h2*3600 + m2*60 + s2
        timedelta = all_second_t1 - all_second_t2
        if(re_type=="h" or re_type=="H"):
            return timedelta/3600.00
        elif(re_type=="m" or re_type=="M"):
            return timedelta/60.00
        else:
            return timedelta

