from django.conf import settings
from django.conf.urls import patterns, url
import views_account
from FeedbackListView import FeedbackListView
from RotaUpdateView import RotaUpdateView
import ManagerView



urlpatterns = patterns(
    "checkapp.views",
    url(r'^gotopage/$', 'gotopage', name='gotopage'),
    url(r'^rotas/$', 'rotas', name='rotas'),
    url(r'^rotassetup/$', 'rotassetup', name='rotassetup'),
    url(r'^rotasdelete/$', 'rotasdelete', name='rotasdelete'),
    url(r'^rotascomfirm/$', 'rotascomfirm', name='rotascomfirm'),
    url(r'^employeeinfo/$', 'employeeinfo', name='employeeinfo'),
    url(r'^report/$', 'report', name='report'),
    url(r'^report_print/$', 'report_print', name='report_print'),
    url(r'^report_note/$', 'report_note', name='report_note'),
    
    url(r'^help/$', 'help', name='help'),
    url(r'^feedback/$', 'feedback', name='feedback'),
    url(r'^list_feedback/$', FeedbackListView.as_view(), name='list_feedback'),
    url(r'^list_feedback/delete/$', 'delete_feedback', name='delete_feedback'),

    url(r'^schedule_work_time/$', 'schedule_work_time', name='schedule_work_time'),
    url(r'^regionalmanager_report/$', 'regionalmanager_report', name='regionalmanager_report'),
    url(r'^regionalmanager/$', 'regionalmanager', name='regionalmanager'),
 
    url(r'^test/$', 'test', name='test'),
    # aiax
    url(r'^rotas/setuprota/$', 'setuprota', name='setuprota'),
    url(r'^loadPic/$', 'loadPic', name='loadPic'),
    url(r'^confirm_modify_button/$', 'confirm_modify_button', name='confirm_modify_button'),

    # rotas
    url(r'^rotas/update/(?P<pk>\d+)/$', RotaUpdateView.as_view(), name='rota_update'),

    # API
    url(r'^loginValidation/$', 'loginValidation', name='loginValidation'),
    url(r'^uploadImageInfoCheckin/$', 'uploadImageInfoCheckin', name='uploadImageInfoCheckin'),
    url(r'^uploadImageInfoCheckout/$', 'uploadImageInfoCheckout', name='uploadImageInfoCheckout'),
    url(r'^getEmployeesInfo/$', 'getEmployeesInfo', name='getEmployeesInfo'),
    
    # lixin
    url(r'^SetManager/$', views_account.Default, name='Default'),
    url(r'^SetManager/GetShopJs/$', views_account.GetShopJs, name='GetShopJs'),
    url(r'^SetManager/GetEmployeesJs/$', views_account.GetEmployeesJs, name='GetEmployeesJs'),
    url(r'^SetManager/SaveDz/$', views_account.SaveDz, name='SaveDz'),
    url(r'^SetManager/SaveScheduledRotas/$', views_account.SaveScheduledRotas, name='SaveScheduledRotas'),

    # get user's check in and check out
    url(r'^sl38doselkklglyhhkoj7580/$', ManagerView.GetSingleUserCheck.as_view(), name='get_single_user_check'),

)