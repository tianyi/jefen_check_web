# -*- coding: utf-8 -*-
from datetime import time
# from checkapp.models import ReportColor
from django.conf import settings


class ReportUnit(object):

    def __init__(self, employee_id, name, day):
        # employee id
        self.employee_id = employee_id
        # employee name
        self.__name = name
        # date
        self.day = day
        self.__schedule_check = None
        self.__checkin = []
        self.__checkout = []
        self.__status = None
        self.__timedeltaCheckIn = None
        self.__timedeltaCheckOut = None
        self.__leaveEarlyType = None
        self.__beLaterType = None

    def getScheduleCheck(self):
        return self.__schedule_check

    def printInfo(self):
        print "name="+self.__name
        print "day="+self.day.__str__()
        print "checkin="+self.__checkin.__str__()
        print "checkout="+self.__checkout.__str__()
        print "employee_id="+self.employee_id
        print "schedule_check="+self.__schedule_check.__str__()


    def getEmployeeID(self):
        return self.employee_id

    def getDay(self):
        # import pdb; pdb.set_trace()
        return self.day.isoformat()

    def setScheduleCheck(self, schedule_check):
        # schedule_check: [s_checkin, s_checkout, rota__rota_type]
        # import pdb; pdb.set_trace()
        # {
        #     'rota_time': [sc['rota__start_time'], sc['rota__end_time']],
        #     'rota_type': sc['rota__rota_type'],
        #     'rota_name': sc['rota__name'],
        #     'report_color_id': sc['rota__report_color_id'],
        # }
        self.__schedule_check = schedule_check

    def setCheckinInfo(self, checkin):
        #self.check_in: [check_time, image_path]
        # import pdb; pdb.set_trace()
        hour, minute, second = checkin[0].split(':')
        t = time(int(hour), int(minute), int(second))
        self.__checkin = [t, checkin[1]]

    def setCheckoutInfo(self, checkout):
        #check_out: [check_time, image_path]
        # import pdb; pdb.set_trace()
        hour, minute, second = checkout[0].split(':')
        t = time(int(hour), int(minute), int(second))
        self.__checkout = [t, checkout[1]]

    def getCheckinInfo(self):
        return self.__checkin

    def getCheckoutInfo(self):
        return self.__checkout

    def getName(self):
        return self.__name

    def getStatus(self):
        # if(not self.__status):
        #     if(self.getDay() == '2015-02-01' and self.employee_id == '00000114'):
        #         self.__status = self.calStatus()
        #     else:
        #         self.__status = [settings.REPORT_STATUS_COLOR['1000']]
        self.__status = self.calStatus()
        return self.__status

    def __getLeaveEarlyType(self):
        if(not self.__leaveEarlyType):
            # print self.getTimedeltaCheckOut()
            if(self.getTimedeltaCheckOut() < 60*60):
                self.__leaveEarlyType = settings.REPORT_STATUS_COLOR['3001']
            else:
                self.__leaveEarlyType = settings.REPORT_STATUS_COLOR['3002']
        return self.__leaveEarlyType

    def __getBeLaterType(self):
        if(not self.__beLaterType):
            if(self.getTimedeltaCheckIn() <= 10*60):
                self.__beLaterType = settings.REPORT_STATUS_COLOR['1001']
            elif(self.getTimedeltaCheckIn() > 10*60 and (self.getTimedeltaCheckIn() <= 20*60)):
                self.__beLaterType = settings.REPORT_STATUS_COLOR['1002']
            elif(self.getTimedeltaCheckIn() > 20*60 and (self.getTimedeltaCheckIn() <= 30*60)):
                self.__beLaterType = settings.REPORT_STATUS_COLOR['1003']
            else:
                self.__beLaterType = settings.REPORT_STATUS_COLOR['1004']
        return self.__beLaterType

    def calStatus(self):
        
        # 1001：迟到：  
        # 1002：正常：  
        # 1003：早退：  
        # 1004：未签到：
        # 1005：未签出：
        # 1006：缺勤：  
        # 1007：未排班：
        # if(self.employee_id == "00000114" and self.day.__str__() == "2015-03-03"):
        #     import pdb; pdb.set_trace()

        # 未排班
        if(self.weipaiban()):
            return [settings.REPORT_STATUS_COLOR['6000']]

        # 上班
        if(self.__schedule_check['rota_type'] == '1'):
            # import pdb; pdb.set_trace()
            if(self.noShow()):
                # if no show, just return, this is no other status.
                return [settings.REPORT_STATUS_COLOR['5000']]

            if(self.onlyNotCheckIn()):
                return [settings.REPORT_STATUS_COLOR['4001']]

            if(self.onlyNotCheckOut()):
                return [settings.REPORT_STATUS_COLOR['4002']]

            if(self.onlyBeLater()):
                return [self.__getBeLaterType()]

            if(self.onlyLeaveEarly()):
                return [self.__getLeaveEarlyType()]

            if(self.notCheckinANDLeaveEarly()):
                return [settings.REPORT_STATUS_COLOR['4001'], self.__getLeaveEarlyType()]

            if(self.beLaterANDNotCheckOut()):
                return [self.__getBeLaterType(), settings.REPORT_STATUS_COLOR['4002']]

            if(self.beLaterANDLeaveEarly()):
                return [self.__getBeLaterType(), self.__getLeaveEarlyType()]

            return [settings.REPORT_STATUS_COLOR['2000']]

        # 非上班
        elif(self.__schedule_check['rota_type'] == '0'):
            # import pdb; pdb.set_trace()
            # print self.__schedule_check['report_color_id']
            try:
                # 公共非上班
                re = settings.REPORT_STATUS_COLOR[str(self.__schedule_check['report_color_id'])]
            except:
                # 自定义非上班
                re = settings.REPORT_STATUS_COLOR['9999']
                re["name"] = self.__schedule_check['rota_name']
            return [re]

        return [settings.REPORT_STATUS_COLOR['1000']]

    def getTimedeltaCheckIn(self):
        if(not self.__timedeltaCheckIn):
            self.__timedeltaCheckIn = self.__timeTimedelta(self.__checkin[0], self.__schedule_check['rota_time'][0])
        return self.__timedeltaCheckIn

    def getTimedeltaCheckOut(self):
        if(not self.__timedeltaCheckOut):
            self.__timedeltaCheckOut = self.__timeTimedelta(self.__schedule_check['rota_time'][1], self.__checkout[0])
        return self.__timedeltaCheckOut

    def __timeTimedelta(self, t1, t2):
        # import pdb; pdb.set_trace()
        h1 = t1.hour
        m1 = t1.minute
        s1 = t1.second

        h2 = t2.hour
        m2 = t2.minute
        s2 = t2.second

        all_second_t1 = h1*3600 + m1*60 + s1
        all_second_t2 = h2*3600 + m2*60 + s2

        return (all_second_t1 - all_second_t2)

    '''
        未排班 > 缺勤 > 上班未打卡 > 下班未打卡 > 上班未打卡+早退 > 迟到+下班未打卡 > 早退+迟到 > 到
    '''
    def beLaterANDNotCheckOut(self):
        return ((not self.__checkout)
           and (self.__checkin)
           and (self.__checkin[0] > self.__schedule_check['rota_time'][0]))

    def notCheckinANDLeaveEarly(self):
        return ((not self.__checkin)
           and (self.__checkout)
           and (self.__checkout[0] < self.__schedule_check['rota_time'][1]))

    def onlyNotCheckIn(self):
        return ((not self.__checkin)
           and (self.__checkout)
           and (self.__schedule_check['rota_time'][1] < self.__checkout[0]))

    def onlyNotCheckOut(self):
        return ((not self.__checkout) 
           and (self.__checkin) 
           and (self.__schedule_check['rota_time'][0] >= self.__checkin[0]))

    def onlyBeLater(self):
        return ((self.__checkin) 
           and (self.__checkout)
           and (self.__checkout[0] >= self.__schedule_check['rota_time'][1])
           and (self.__checkin[0] >= self.__schedule_check['rota_time'][0]))

    def onlyLeaveEarly(self):
        return ((self.__checkin) 
           and (self.__checkout)
           and (self.__checkin[0] <= self.__schedule_check['rota_time'][0])
           and (self.__checkout[0] <= self.__schedule_check['rota_time'][1]))

    def noShow(self):
        # import pdb; pdb.set_trace()
        b = (not self.__checkin) and (not self.__checkout)
        # print b
        return b

    def weipaiban(self):
        return not self.__schedule_check

    def beLaterANDLeaveEarly(self):
        return ((self.__checkin) 
           and (self.__checkout)
           and (self.__checkin[0] > self.__schedule_check['rota_time'][0])
           and (self.__checkout[0] < self.__schedule_check['rota_time'][1]))

    def __str__(self):
        return '————'

