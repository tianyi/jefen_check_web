# -*- coding: utf-8 -*- 

from django.shortcuts import render
# from models import Other2
from models import Rotas, Employees, ScheduledRotas, ImageForm, Checkin, Checkout, MyUser, RotasForm, MonthReportNoteForm, MonthReportNote
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from utils import *
from django.http import HttpResponseRedirect, HttpResponse
import simplejson
from datetime import datetime, date
from django.views.decorators.csrf import csrf_exempt
import calendar
from ReportUnit import ReportUnit
from StatisticsReport import StatisticsReport
from django.conf import settings
from datetime import datetime
from django.views.generic import TemplateView, RedirectView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from datetime import time
from django.db.models import Q

from django.core import serializers

# Create your views here.

@login_required
def Default(request):
    # import pdb; pdb.set_trace()

    # sql = "Select id,shop_id,manager_id,shop_name,scheduledRotas From checkapp_myuser"
    # Rs = MyUser.objects.raw(sql)[:3]
    return render_to_response(
        'checkapp/SetManager.html',
        # 'checkapp/wlx.html',
        {},
        context_instance=RequestContext(request),
    )


def GetShopJs(request):

    #条件过滤
    # json_data = serializers.serialize("json", MyUser.objects.filter(username = "350078238"))

    #指定数据条数
    # json_data = serializers.serialize("json", MyUser.objects.all()[:1]) 
    # 这是查找从第5个到第10个之间的数据。 
    # json_data = serializers.serialize("json", MyUser.objects.all()[5:10])

    # data = [{"id": 0, "name": "Item 0", "price": "$0"}]
    # #加上 [] 即可为 [{"price": "$0", "id": 0, "name": "Item 0"}] 结果
    # return HttpResponse(simplejson.dumps(data), content_type='application/json') 
   

    #ORM查询
    # json_data = MyUser.objects.filter(user_type=1).values('id','shop_id','manager_id','shop_name','scheduledRotas')[:1]
    # print type(json_data)
    # import pdb; pdb.set_trace() <class 'django.db.models.query.ValuesQuerySet'>
    # x = [ {"id": i["id"],"shop_id":i["shop_id"],"manager_id":i["manager_id"], "name": i["shop_name"], "price": i["scheduledRotas"] } for i in json_data]
    # return HttpResponse(simplejson.dumps(x), content_type='application/json')

    #自定义查询
    # import pdb; pdb.set_trace()
    sql = "Select a.id,a.shop_id,a.manager_id,a.shop_name,a.scheduledRotas,b.name From checkapp_myuser a left join \
          checkapp_employees b on a.manager_id = b.employee_id Where a.user_type = 1 order by a.manager_id desc,a.shop_name desc"
    # print sql
    json_data = MyUser.objects.raw(sql)#[:5]
    #print type(json_data)  #<class 'django.db.models.query.RawQuerySet'>
    
    x = [ {"shop_id":i.shop_id ,"manager_id":i.manager_id, "shop_name": i.shop_name, "scheduledRotas": i.scheduledRotas,"name":i.name} for i in json_data]
    return HttpResponse(simplejson.dumps(x), content_type='application/json')

@csrf_exempt
def GetEmployeesJs(request):
    #自定义查询
    Shop_ID = request.GET.get("ShopID") #当前店铺编号
    SetShop = request.GET.get("SetShopID") #设置店铺编号 
    # sql = "select a.employee_id,a.shop_id,a.name,b.shop_name,b.manager_id from checkapp_employees a inner join checkapp_myuser b \
    #       on a.shop_id = b.shop_id where a.shop_id = '" + Shop_ID + "'"

    sql = "select a.employee_id,a.shop_id,a.name,b.shop_name,(Select manager_id From checkapp_myuser Where \
          shop_id ='" + str(SetShop) + "') manager_id from checkapp_employees a inner join  checkapp_myuser b on a.shop_id = b.shop_id \
          where a.shop_id = '" + Shop_ID + "' and a.status=0"

    json_data = Employees.objects.raw(sql)
    x = [ {"employee_id": i.employee_id,"name":i.name,"shop_id":i.shop_id,"shop_name":i.shop_name,"manager_id":i.manager_id} for i in json_data]
    # print type(json_data) # <class 'django.db.models.query.RawQuerySet'>
    # import pdb; pdb.set_trace()
    return HttpResponse(simplejson.dumps(x), content_type='application/json')

def SaveDz(request):
    EmployeeID = request.POST.get("EmployeeID")
    ShopID     = request.POST.get("ShopID")    
    try:
        MyUser.objects.filter(shop_id=ShopID).update(manager_id=EmployeeID,scheduledRotas=0)
        Rs = Employees.objects.filter(employee_id=EmployeeID).values('name')
        JsonStr = {"Results":True,"name": Rs[0]["name"]}
    except:  
        JsonStr = {"Results":False,"MSG":u"设置店长失败！\n请于管理员联系!"}
    # import pdb; pdb.set_trace()
    return HttpResponse(simplejson.dumps(JsonStr), content_type='application/json')

@csrf_exempt
def SaveScheduledRotas(request):
    EmployeeID = request.POST.get("EmployeeID") #店长ID
    ShopID     = request.POST.get("ShopID")     #店铺ID
    #先查询出店长原来在哪个店铺排班
    OldShopID = MyUser.objects.filter(manager_id = EmployeeID).filter(scheduledRotas=1).values('shop_id')
    if (len(OldShopID) ==0):
        UpButon = "NO"
    else:
        UpButon = OldShopID[0]['shop_id']

    try:
        MyUser.objects.filter(manager_id = EmployeeID).update(scheduledRotas=0)
        MyUser.objects.filter(shop_id=ShopID).filter(manager_id = EmployeeID).update(scheduledRotas=1)
        JsonStr = {"Results":True,"UpButon":UpButon}
    except:
        JsonStr = {"Results":False,"MSG":u"保存数据失败！\n请于管理员联系!"}
    # import pdb; pdb.set_trace()
    return HttpResponse(simplejson.dumps(JsonStr), content_type='application/json')