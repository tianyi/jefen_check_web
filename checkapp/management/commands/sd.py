# -*- coding: utf-8 -*- 

from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from Database import ODBC_MS
from checkapp.models import MyUser, Employees
from django.conf import settings
import logging.config
import logging


'''
    Tianyi Dong
    Custom management commands are especially useful for running 
    standalone scripts or for scripts that are periodically 
    executed from the UNIX crontab or from Windows scheduled tasks control panel.
'''


logger = logging.getLogger('sd')

class Command(BaseCommand):
    # import pdb; pdb.set_trace()

    # logging.config.fileConfig('logging.conf')
    # self.logger = logging.getLogger('sd')
    # logger = logging.getLogger('sd')
    # def __init__(self):
    #     super.__init__(self)
    #     self.logger = logging.getLogger('sd')

    option_list = BaseCommand.option_list + (
        make_option('--all',
            action='store_true',
            dest='all',
            default=False,
            help='sync data from ms db to sqlite3'),
        )

    def handle(self, *args, **options):
        # logging.config.fileConfig('logging.conf')
        # self.logger = logging.getLogger('sd')
        # ...
        if options['all']:
            # do somgthing
            self.stdout.write("Start sync all data...")

            self.odbc_ms = ODBC_MS(
                settings.DATABASES_MS['DRIVER'], 
                settings.DATABASES_MS['SERVER'], 
                settings.DATABASES_MS['DATABASE'], 
                settings.DATABASES_MS['UID'], 
                settings.DATABASES_MS['PWD']
            )

            self.__syncDataFromMS()
            
            # self.stdout.write("option_list="+option_list, ending='')
        # ...

    def __syncDataFromMS(self):
        # import pdb; pdb.set_trace()

        userData = self.__getShopFromMS()
        userData = self.__syncEmployeeInfo()
        # for ud in userData:
        # user = MyUser.objects.create_user(ud['id'], password=ud['passwd'])
        # At this point, user is a User object that has already been saved
        # to the database. You can continue to change its attributes
        # if you want to change other fields.
        # user.last_name = 'Lennon'
        # user.save()

    def __syncEmployeeInfo(self):
        logger.info("start sync employee data!")
        # sql_employee = "exec GetEmployeeInfo"
        sql_employee = "select * from  GetEmployeeInfo"
        re = self.odbc_ms.ExecQuery(sql_employee)
        # set is updated to false
        Employees.objects.filter().update(is_updated=0)
        # import pdb; pdb.set_trace()
        for rx in re:
            # 1 id， 姓名， 店铺id， 在职状态
            # logging.debug(rx)
            # ('00000089', '\xd0\xec\xbe\xb2\xc8\xe7', 'BJ006', False)
            try:
                name = rx[1].decode('gbk')
                
            except:
                logger.info("----------something wrong---------")
                logger.info(rx)
                logger.info("----------------------------------")
                continue
            logger.debug(str(rx[0]) +","+ name + "," + str(rx[2]) + "," + str(rx[3]))
            employee, is_created = Employees.objects.get_or_create(employee_id=rx[0])
            employee.shop_id = rx[2]
            employee.name = name
            employee.is_updated = 1
            employee.status = rx[3]
            employee.save()

        count = Employees.objects.filter(is_updated=0).exclude(shop_id=settings.REGIONAL_MANAGER_SHOPNAME)
        if(count > 0):
            Employees.objects.filter(is_updated=0).exclude(shop_id=settings.REGIONAL_MANAGER_SHOPNAME).update(status=1)
            # self.logger.info(str(count) + " employee set to status = 0")
        logger.info("end sync employee data!")
        # self.logger.info("end sync employee data!")


    def __getShopFromMS(self):
        logger.info("shop info:")
        
        # sql_shop = "exec GetShopInfo"
        sql_shop = "select * from  GetShopInfo"
        # getEmployeeList = "exec GetEmployeeList 'bj001'"
        MyUser.objects.filter(user_type=1).update(is_opened=0)
        # s = datetime.now()
        re = self.odbc_ms.ExecQuery(sql_shop)
        # with open('shopinfo', 'wb') as f:
        #     for rxx in re:
        #         f.write(str(rxx))
        for rx in re:
            # import pdb; pdb.set_trace()
            
            # ('p001', '\xb1\xb1\xbe\xa9\xc8\xfc\xcc\xd8', 'P043', '6512', 1)
            # 1 id， 姓名， 店铺id，密码, 在职状态
            # 用户名，店铺名称，店铺id, 密码, status
            try:
                shop_name = rx[1].decode('gb18030')
                # self.stdout.write(rx)
            except:
                logger.error("----------something wrong---------")
                logger.error(rx)
                logger.error("----------------------------------")
                continue
            logger.debug(str(rx[0]) + "," + str(rx[3]) + ","+ str(rx[2]) + shop_name + "," + str(rx[4]))
            try:
                user = MyUser.objects.get(username=rx[0])
                user.set_password(rx[3])
            except:
                user = MyUser.objects.create_user(rx[0], password=rx[3])
            user.shop_name = shop_name
            user.user_type = 1
            user.is_opened = 1
            user.shop_id = rx[2]
            user.shop_status = rx[4]
            user.save()
        logger.info("End sync shop data!")
        
