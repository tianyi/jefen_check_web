# -*- coding: utf-8 -*-

import sqlite3
import pyodbc
from datetime import datetime
import logging




class Database():

    def __init__(self):
        self.logger = logging.getLogger('Database')

class ODBC_MS(Database):
    '''
        对pyodbc库的操作进行简单封装
        pyodbc库的下载地址:http://code.google.com/p/pyodbc/downloads/list
        使用该库时，需要在Sql Server Configuration Manager里面将TCP/IP协议开启
        此类完成对数据库DB的连接/查询/执行操作
        正确的连接方式如下:
        cnxn = pyodbc.connect(DRIVER='{SQL SERVER}',SERVER=r'ZHANGHUAMIN\MSSQLSERVER_ZHM',DATABASE='AdventureWorks2008',UID='sa',PWD='wa1234',charset="utf-8")
    '''
    def __init__(self, DRIVER, SERVER, DATABASE, UID, PWD):

        '''
            initialization
        '''
        Database.__init__(self)
        self.DRIVER = DRIVER
        self.SERVER = SERVER
        self.DATABASE = DATABASE
        self.UID = UID
        self.PWD = PWD

    def __GetConnect(self):
        ''' Connect to the DB '''

        if not self.DATABASE:
            raise(NameError, "no setting db info")
        # self.conn = pyodbc.connect(DRIVER=self.DRIVER, SERVER=self.SERVER, DATABASE=self.DATABASE, UID=self.UID, PWD=self.PWD, charset="UTF-8")
        # self.conn = pyodbc.connect(DRIVER=self.DRIVER, SERVER=self.SERVER, DATABASE=self.DATABASE, UID=self.UID, PWD=self.PWD)
        # self.conn = pyodbc.connect(DRIVER=self.DRIVER, SERVER=self.SERVER, DATABASE=self.DATABASE, UID=self.UID, PWD=self.PWD)
        #self.conn = pyodbc.connect(DRIVER=self.DRIVER, SERVER=self.SERVER, DATABASE=self.DATABASE, UID=self.UID, PWD=self.PWD)

        conn_str = ('DRIVER={%s};SERVER=%s;DATABASE=%s;UID=%s;PWD=%s' % (self.DRIVER, self.SERVER, self.DATABASE, self.UID, self.PWD))

        # self.conn = pyodbc.connect('DRIVER={SQL SERVER};SERVER=192.168.0.243;DATABASE=CheckInOut;UID=sa;PWD=NewSql2014-11-20')
        self.conn = pyodbc.connect(conn_str)
        cur = self.conn.cursor()
        if not cur:
            raise(NameError, "connected failed!")
        else:
            return cur

    def ExecQuery(self, sql, parameters=()):
        ''' Perform one Sql statement '''

        cur = self.__GetConnect()  # 建立链接并创建数据库操作指针
        cur.execute(sql, parameters)  # 通过指针来执行sql指令
        ret = cur.fetchall()  # 通过指针来获取sql指令响应数据
        cur.close()  # 游标指标关闭
        self.conn.close()  # 关闭数据库连接
        return ret

    def ExecNoQuery(self, sql, parameters=()):
        ''' Person one Sql statement like write data, or create table, database and so on'''

        cur = self.__GetConnect()
        cur.execute(sql, parameters)
        self.conn.commit()  # 连接句柄来提交
        cur.close()
        self.conn.close()

    # parameters = [(a, b,), ]
    def ExecInsertMany(self, sql, parameters=[]):

        cur = self.__GetConnect()
        cur.executemany(sql, parameters)
        self.conn.commit()
        cur.close()
        self.conn.close()


if __name__ == '__main__':

    odbc_ms = ODBC_MS(
        DATABASES_MS['DRIVER'],
        DATABASES_MS['SERVER'],
        DATABASES_MS['DATABASE'],
        DATABASES_MS['UID'],
        DATABASES_MS['PWD']
    )

    # sql = 'Select * From Test'

    # # import pdb; pdb.set_trace()

    # sql = "exec VerifyLogin 'p043','123456'"

    # getEmployeeList = "exec GetEmployeeList 'bj001'"

    # s = datetime.now()
    # re = odbc_ms.ExecQuery(getEmployeeList)
    # s1 = datetime.now()

    # x = s1 - s
    # print x

    # i = 1 + 1
    # for i in re:
    #     print i

    li = [
        'create table rotas ( \
                ID integer primary key, \
                start_time DATETIME, \
                end_time DATETIME, \
                name varchar(50), \
                css_clour varchar(50), \
                default_display bool)',

        'delete from rotas',

        'create table check_in ( \
        ID integer primary key autoincrement, \
        checkin_time DATETIME, \
        employee_id varchar(50) , \
        isUploaded bool, \
        check_pic blob, \
        FOREIGN KEY(employee_id) REFERENCES employee(id) )',

        'delete from check_in',

        'create table if not exists check_out ( \
            ID integer primary key autoincrement, \
            checkout_time DATETIME, \
            employee_id varchar(50) , \
            isUploaded bool, \
            check_pic blob, \
            FOREIGN KEY(employee_id) REFERENCES employee(id) )',

        'delete from check_out',
    ]

    for i in li:
        print i
        # re = odbc_ms.ExecNoQuery(i)
        

