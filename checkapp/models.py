# -*- coding: utf-8 -*- 

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from datetime import datetime



class Employees(models.Model):
    employee_id = models.CharField(u'用户ID', primary_key=True, max_length=100)
    name = models.CharField(u'姓名', max_length=50)
    shop_id = models.CharField(u'店铺ID', max_length=50)
    # 0: 在职 1: 离职
    status = models.BooleanField(u'在职状态', default=True)
    is_updated = models.IntegerField(u'已更新', default=0)

    def __unicode__(self):
        return self.name

class EmployeesForm(ModelForm):

    class Meta:
        model = Employees
        fields = ('employee_id', 'name', )

# Create your models here.
class MyUser(AbstractUser):
    user_type = models.IntegerField(u'用户类型', default=0)
    shop_name = models.CharField(u'店铺名称', max_length=50)
    shop_id = models.CharField(u'店铺ID', max_length=50)
    shop_status = models.IntegerField(u'店铺状态', default=1)
    edit_rotas = models.BooleanField(u'班次编辑', default=1)
    manager = models.ForeignKey(Employees)
    scheduledRotas = models.IntegerField(u'店长在本店排班', default=0)

    is_opened = models.IntegerField(u'已更新', default=1)


    def __unicode__(self):
        return self.username + '-' + self.shop_name

class FeedBack(models.Model):
    user = models.ForeignKey(MyUser)
    desc = models.CharField(u'描述', max_length=400)
    created_time = models.DateTimeField(u'创建时间', default=datetime.now())

class FeedBackForm(ModelForm):
    # import pdb; pdb.set_trace()
    desc = forms.CharField(label=u'', widget=forms.Textarea(attrs={'cols': 80, 'rows': 5, 'placeholder': u'在这里输入'}))
    class Meta:
        model = FeedBack
        fields = ('desc',)


class Rotas(models.Model):
    user = models.ForeignKey(MyUser)
    name = models.CharField(u'班次名称', max_length=50)
    rota_type = models.CharField(u'班次类型(上班1,非上班0)', max_length=1, default=1)
    is_common = models.IntegerField(u'公共', default=0)
    report_color_id = models.CharField(u'报表颜色ID', default="0000", max_length=50)
    start_time = models.TimeField(u'开始时间', default="08:00")
    end_time = models.TimeField(u'结束时间', default="08:00")
    color = models.CharField(u'显示颜色', max_length=20)
    created_time = models.DateTimeField(u'创建时间', default=datetime.now())

    def __unicode__(self):
        return str(self.id)  + " . " +  self.name + "--" + self.user.username + '--' + self.user.shop_name

# class ReportColor(models.Model):
#     '''
#         1001：迟到：  insert into checkapp_reportcolor values(1, '迟', '#d06b64', 1001)
#         1002：正常：  insert into checkapp_reportcolor values(2, '到', '#fa573c', 1002)
#         1003：早退：  insert into checkapp_reportcolor values(3, '早', '#fa573c', 1003)
#         1004：未签到：insert into checkapp_reportcolor values(4, '未签到', '#fa573c', 1004)
#         1005：未签出：insert into checkapp_reportcolor values(5, '未签出', '#fa573c', 1005)
#         1006：缺勤：  insert into checkapp_reportcolor values(6, '缺', '#fa573c', 1006)
#         1007：未排班：insert into checkapp_reportcolor values(7, '未排班', '#fa573c', 1007)
#     '''
#     report_type = models.CharField(u'出勤类型ID',  max_length=20)
#     name = models.CharField(u'名称', max_length=20)
#     color = models.CharField(u'显示颜色', max_length=20)

class ScheduledRotas(models.Model):
    employee = models.ForeignKey(Employees)
    rota = models.ForeignKey(Rotas)
    # 0-6
    weekdayNum = models.IntegerField()
    scheduled_day = models.DateField()

    def __unicode__(self):
        return self.employee.name + "-" + self.scheduled_day.__str__()

def getFilePath(instance, filename):

    # import pdb; pdb.set_trace()
    date = instance.check_time
    date = date.replace(":", "-")

    filename = instance.__class__.__name__ + "/" + instance.employee.shop_id + "/" + instance.check_day + "/" + instance.employee_id + "_" + date + ".jpg"
    return filename


class Checkin(models.Model):
    employee = models.ForeignKey(Employees)
    check_day = models.DateField()
    check_time = models.TimeField()
    image_path = models.ImageField(upload_to=getFilePath, max_length=255)
    shop_id = models.CharField(u'店铺ID', max_length=50)

    def __unicode__(self):
        return self.employee.name + "-" + self.check_day.__str__() + " " + self.check_time.__str__()


class Checkout(models.Model):
    employee = models.ForeignKey(Employees)
    check_day = models.DateField()
    check_time = models.TimeField()
    image_path = models.ImageField(upload_to=getFilePath, max_length=255)
    shop_id = models.CharField(u'店铺ID', max_length=50)

    def __unicode__(self):
        return self.employee.name + "-" + self.check_day.__str__() + " " + self.check_time.__str__()


class ImageForm(ModelForm):
    class Meta:
        model = Checkin
        fields = ('image_path', )

class MonthReportNote(models.Model):
    # 201501 = Yesr + month
    report_id = models.CharField(u'报表ID', max_length=20)
    note = models.CharField(u'添加备注', null=False, max_length=1000)
    created_time = models.DateTimeField(u'创建时间', default=datetime.now())
    user = models.ForeignKey(MyUser)
    # 1 commit 0 can edit
    commited = models.IntegerField(u'确认', default=0)

    def __unicode__(self):
        return self.user.username +"-"+ self.report_id

class MonthReportNoteForm(ModelForm):
    # import pdb; pdb.set_trace()
    # note = forms.CharField(label=u'备注', widget=forms.Textarea(attrs={'cols': 80, 'rows': 3, 'placeholder': u'备注'}))
    class Meta:
        model = MonthReportNote
        # fields = ('name', 'rota_type', 'start_time', 'end_time', 'color')
        fields = ('note',)


 
# END_TIME_CHOICE = ( 
#     ('', u"选择时间"), 
#     (2, u"2"),         
#     (4, u"4"),         
#     (8, u"8"), 
#     (16, u"16"), 
# ) 

rota_type_choice = (
    (1, u'上班'),
    (0, u'非上班'),
)

COLOR_CHOICE = (
    ("#16a765", "#16a765"),
    ("#fad165", "#fad165"),
    ("#ac725e", "#ac725e"),
    ("#d06b64", "#d06b64"),
    ("#f83a22", "#f83a22"),
    ("#fa573c", "#fa573c"),
    ("#ff7537", "#ff7537"),
    ("#ffad46", "#ffad46"),
    ("#42d692", "#42d692"),
    ("#7bd148", "#7bd148"),
    ("#b3dc6c", "#b3dc6c"),
    ("#fbe983", "#fbe983"),
    ("#92e1c0", "#92e1c0"),
    ("#9fe1e7", "#9fe1e7"),
    ("#9fc6e7", "#9fc6e7"),
    ("#4986e7", "#4986e7"),
    ("#9a9cff", "#9a9cff"),
    ("#b99aff", "#b99aff"),
    ("#c2c2c2", "#c2c2c2"),
    ("#cabdbf", "#cabdbf"),
    ("#cca6ac", "#cca6ac"),
    ("#f691b2", "#f691b2"),
    ("#cd74e6", "#cd74e6"),
    ("#a47ae2", "#a47ae2"),
)

class RotasForm(ModelForm):
    rota_type = forms.ChoiceField(
        label=u"班次类型",
        required=True,
        choices=rota_type_choice,
        widget=forms.RadioSelect(),
        initial='1',
    )
    color = forms.ChoiceField(label=u"显示颜色", required=True, choices=COLOR_CHOICE)
    class Meta:
        model = Rotas
        # fields = ('name', 'rota_type', 'start_time', 'end_time', 'color')
        fields = ('id','name', 'rota_type', 'start_time', 'end_time', 'color')

