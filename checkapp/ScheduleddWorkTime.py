# -*- coding: utf-8 -*-
from models import *
from utils import getMonthStartEnd, timeTimedelta
from datetime import date
from collections import defaultdict

class ScheduleddWorkTime():
    
    def __init__(self, shop_id, year, month):
        self.shop_id = shop_id
        self.year = year
        self.month = month

    '''
        姓名    已排班总时长
    '''

    def generateReport(self):
        return self.__generateReportImpl()

    def __generateReportImpl(self):
        return self.__query_scheduled_time()

    def __query_scheduled_time(self):
        # import pdb; pdb.set_trace()
        monthRange = getMonthStartEnd(self.year, self.month)
        start = monthRange[0]
        end = monthRange[1]
        monthDayCount = monthRange[2]

        start_date = date(self.year, self.month, 1)
        end_date = date(self.year, self.month, monthDayCount)

        shop = MyUser.objects.get(shop_id=self.shop_id)
        # 店长在本店排班
        if(shop.scheduledRotas):
            employees = Employees.objects.filter(Q(shop_id=self.shop_id, status=0) | Q(employee_id=shop.manager_id))
        else:
            employees = Employees.objects.filter(shop_id=self.shop_id, status=0).exclude(employee_id=shop.manager_id)
        # employees = Employees.objects.filter(shop_id=shop_id, status=0)
        employees_list = [e.employee_id for e in employees]

        schedule_check = ScheduledRotas.objects.filter(
            employee_id__in=employees_list, 
            scheduled_day__range=(start_date, end_date),
            rota__is_common=0,
        ).values(
            "employee_id",
            "rota__name",
            "rota__rota_type",
            "rota__start_time",
            "rota__end_time",
            "weekdayNum",
            "scheduled_day",
        )
        # for mm in schedule_check:
        #     print mm
        # print schedule_check
        # import pdb; pdb.set_trace()

        dd = defaultdict(list)
        # i = 0
        for sc in schedule_check:
            # import pdb; pdb.set_trace()
            time_delta = timeTimedelta(sc['rota__end_time'], sc['rota__start_time'], "h")
            # if(sc['employee_id'] == '10000090'):
                # import pdb; pdb.set_trace()
                # print "--------------"
                # print sc["scheduled_day"]
                # print time_delta
                # print sc["rota__end_time"]
                # print sc["rota__start_time"]
                # print "--------------"
                # i = i + 1
            if(not dd[sc['employee_id']]):
                dd[sc['employee_id']] = time_delta
            else:
                dd[sc['employee_id']] = dd[sc['employee_id']] + time_delta
        # print i

        result = []
        for em in employees:
         
            if(not dd[em.employee_id]):
                time = 0
            else:
                time = dd[em.employee_id]
            # print time
            result.append({
                "employee_id": em.employee_id,
                "total_time": '%.2f' % time,
                "name": em.name,
            })

        # import pdb; pdb.set_trace()
        # print result
        return result
        # return [{"name": "name", "total_time": 189}, {"name": "name", "total_time": 189}, {"name": "name", "total_time": 189}]
