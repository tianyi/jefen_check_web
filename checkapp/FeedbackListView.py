from django.views.generic.list import ListView
from django.utils import timezone

from models import FeedBack

class FeedbackListView(ListView):

    model = FeedBack
    

    template_name = "checkapp/list_feedback.html"

    def get_context_data(self, **kwargs):
        # import pdb; pdb.set_trace()
        context = super(FeedbackListView, self).get_context_data(**kwargs)
        # context['now'] = timezone.now()
        return context