# -*- coding: utf-8 -*-
from datetime import time
# from checkapp.models import ReportColor
from django.conf import settings


class RMReportUnit(object):

    def __init__(self, employee_id, name, day):
        # employee id
        self.employee_id = employee_id
        self.name = name
        self.day = day
        self.__checkin = ['', '']
        self.__checkout = ['', '']

    def generate_report(self):
        return "xxx"

    def get_name(self):
        return self.name

    def get_checkin(self):
        return self.__checkin

    def get_checkout(self):
        return self.__checkout

    def get_check_day(self):
        return self.day.isoformat()

    def get_eid(self):
        return self.employee_id

    def set_checkin(self, checkin):
        #self.check_in: [check_time, image_path]
        # import pdb; pdb.set_trace()
        hour, minute, second = checkin[0].split(':')
        t = time(int(hour), int(minute), int(second))
        self.__checkin = [t, checkin[1]]

    def set_checkout(self, checkout):
        #check_out: [check_time, image_path]
        # import pdb; pdb.set_trace()
        hour, minute, second = checkout[0].split(':')
        t = time(int(hour), int(minute), int(second))
        self.__checkout = [t, checkout[1]]