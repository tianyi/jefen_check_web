# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from django.views.generic.edit import UpdateView
from checkapp.models import Rotas, RotasForm
from django.forms.models import modelform_factory
from django import forms
from checkapp.models import rota_type_choice, COLOR_CHOICE


class RotaUpdateView(UpdateView):
    model = Rotas
    # fields = ['name', 'rota_type', 'start_time', 'end_time', 'color']
    template_name_suffix = '_update_form'
    # success_url = reverse('report')
    # import pdb; pdb.set_trace()
    # form_class =  modelform_factory(Rotas,
    #                                 fields=fields,
    #                                 widgets={
    #                                     "rota_type": forms.RadioSelect(
    #                                         choices=rota_type_choice,
    #
    #                                     ),
    #                                     # "color": forms.ChoiceField(
    #                                     #     choices=COLOR_CHOICE,
    #                                     # ),
    #                                 }
    # )
    # import pdb; pdb.set_trace()

    form_class = RotasForm

    def get_success_url(self):
        return "/check/rotassetup/"

    def get_context_data(self, **kwargs):
        context = super(RotaUpdateView, self).get_context_data(**kwargs)
        context['user_type'] = self.request.user.user_type
        
        # import pdb; pdb.set_trace()
        return context
