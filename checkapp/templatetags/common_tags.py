from django import template

register = template.Library()

@register.simple_tag
def active(request, pattern):
    # import pdb; pdb.set_trace()
    import re
    if re.search(pattern, request.path):
        return 'active'
    return ''