from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView, RedirectView
from django.contrib import admin



urlpatterns = patterns(

    "",
    # url(r"^$", TemplateView.as_view(template_name="homepage.html"), name="home"),
    # url(r"^$", "checkapp.views.homepage", name="home"),
    
    url(r'^$', RedirectView.as_view(url='/account/login/'), name="home"),
    url(r"^check/", include("checkapp.urls")),
    url(r"^admin/", include(admin.site.urls)),
    url(r"^account/", include("account.urls")),
    url(r"^ipad/", include("ipad.urls")),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# print urlpatterns
