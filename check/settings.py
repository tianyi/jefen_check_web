# -*- coding: utf-8 -*-

import os


PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
PACKAGE_ROOT = os.path.abspath(os.path.dirname(__file__))
BASE_DIR = PACKAGE_ROOT

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "dev.db",
    }
}

DATABASES_MS = {
    'DRIVER': 'SQL SERVER',
    'SERVER': '192.192.192.243',
    'DATABASE': 'CheckInOut',
    'UID': 'sa',
    'PWD': 'NewSql2014-11-20'
}

ALLOWED_HOSTS = ["*"]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = "UTC"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "zh-CN"

SITE_ID = int(os.environ.get("SITE_ID", 1))

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = "/media/"

# Absolute path to the directory static files should be collected to.
# Don"t put anything in this directory yourself; store your static files
# in apps" "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PACKAGE_ROOT, "site_media", "static")

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = "/static/"

LOG_ROOT = os.path.join(PACKAGE_ROOT, "logs")

# Additional locations of static files
STATICFILES_DIRS = [
    os.path.join(PACKAGE_ROOT, "static"),
]

DEFAULT_CHARSET = 'utf8' 

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# Make this unique, and don't share it with anybody.
SECRET_KEY = "(=+-%=)e3w*-0wizkht47ca4^wmgl+&4&g2j_kt*1xm*w$6^*s"

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = [
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
]

TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    "account.context_processors.account",
    "pinax_theme_bootstrap.context_processors.theme",
]


MIDDLEWARE_CLASSES = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.auth.middleware.SessionAuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",

    # pagination
    'pagination.middleware.PaginationMiddleware',
]

ROOT_URLCONF = "check.urls"

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "check.wsgi.application"

TEMPLATE_DIRS = [
    os.path.join(PACKAGE_ROOT, "templates"),
]

INSTALLED_APPS = [
    # 'django_admin_bootstrapped',
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.staticfiles",

    # theme
    "bootstrapform",
    "pinax_theme_bootstrap",

    # external
    "account",
    "eventlog",
    "metron",
    "pagination",
    'djangoformsetjs',
    'widget_tweaks',
    'nested_inline',

    # project
    "check",
    'checkapp',
    "ipad",
    "rest_framework",
    'rest_framework.authtoken',
]

REST_FRAMEWORK = {
    
    'PAGE_SIZE': 10,
    'PAGINATE_BY': 10,
    'PAGINATE_BY_PARAM': 'page_size',
    'MAX_PAGINATE_BY': 100,

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        # 'rest_framework.authentication.TokenAuthentication',
    ),

    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
        # 'rest_framework.permissions.AllowAny',
    ),

    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',

    'DEFAULT_RENDERER_CLASSES': (
        # 'rest_framework.renderers.JSONRenderer',
        'ipad.MyJSONRenderer.MyJSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_API_ROOT': 'data',

    'UNICODE_JSON': True,

    'FORMAT_DATATIME': "%Y.%m.%d %H:%M",

    'FORMAT_DATE': "%Y.%m.%d",

}

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse"
        }
    },

    'formatters': {
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },

    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler"
        },
        'log_to_stdout': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
            },

        'sd_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOG_ROOT,'sd.log'),
            'maxBytes': 1024*1024*2,
            'backupCount': 5,
            'formatter': 'simple',
            },
    },

    'loggers': {
        'sd': {
            'handlers': ['sd_file', 'log_to_stdout'],
            'level': 'DEBUG',
            'propagate': True,
        }
    }
}

APPEND_SLASH = True

FIXTURE_DIRS = [
    os.path.join(PROJECT_ROOT, "fixtures"),
]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

LOGIN_URL="/account/login/"

ACCOUNT_OPEN_SIGNUP = False
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = False
ACCOUNT_LOGIN_REDIRECT_URL = "gotopage"
ACCOUNT_LOGOUT_REDIRECT_URL = "home"
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 2
HELP_URL = "help"


AUTHENTICATION_BACKENDS = [
    "account.auth_backends.UsernameAuthenticationBackend",
]

AUTH_USER_MODEL = "checkapp.MyUser"

REGIONAL_MANAGER_SHOPNAME = "REGIONAL_MANAGER_SHOP"

CLIENT_VERSION = "clientv1"

REPORT_START_YEAR = 2015

DISPLAY_YEAR_NUMBER = 7

MONTH_LIST = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]


# 1001：迟到：  insert into checkapp_reportcolor values(1, '迟', '#d06b64', 1001)
# 1002：正常：  insert into checkapp_reportcolor values(2, '到', '#fa573c', 1002)
# 1003：早退：  insert into checkapp_reportcolor values(3, '早', '#fa573c', 1003)
# 1004：未签到：insert into checkapp_reportcolor values(4, '未签到', '#fa573c', 1004)
# 1005：未签出：insert into checkapp_reportcolor values(5, '未签出', '#fa573c', 1005)
# 1006：缺勤：  insert into checkapp_reportcolor values(6, '缺', '#fa573c', 1006)
# 1007：未排班：insert into checkapp_reportcolor values(7, '未排班', '#fa573c', 1007)
# {rx.report_type: {"name": rx.name, "color": rx.color}}

# 1=checkin 2=checkout 0=other 3=checkin+checkout
REPORT_STATUS_COLOR = {
    "1000": {"name": u'出错',    "color": "red",     "id": "1000", "check_type": 0},
    "1001": {"name": u'迟1',     "color": "#b3dc6c", "id": "1001", "check_type": 1}, # 迟到10分钟之内
    "1002": {"name": u'迟2',     "color": "#b3dc6c", "id": "1002", "check_type": 1}, # 迟到10~20分钟之间
    "1003": {"name": u'迟3',     "color": "#b3dc6c", "id": "1003", "check_type": 1}, # 迟到20~30分钟之间
    "1004": {"name": u'迟4',     "color": "#b3dc6c", "id": "1004", "check_type": 1}, # 迟到超过30分钟
    "2000": {"name": u'到',      "color": "",        "id": "2000", "check_type": 1}, # 到
    "3001": {"name": u'早1',     "color": "#fad165", "id": "3001", "check_type": 2}, # 早退一小时以内
    "3002": {"name": u'早2',     "color": "#fad165", "id": "3002", "check_type": 2}, # 早退超过一小时
    "4001": {"name": u'未1',     "color": "#92e1c0", "id": "4001", "check_type": 1}, # 上班未打卡
    "4002": {"name": u'未2',     "color": "#9fe1e7", "id": "4002", "check_type": 2}, # 下班未打卡
    "5000": {"name": u'缺',      "color": "#FF0000", "id": "5000", "check_type": 3}, # 缺勤
    "6000": {"name": u'',        "color": ""       , "id": "6000", "check_type": 1},  # 未排班

    "7001": {"name": u'休',        "color": ""       , "id": "7001", "check_type": 0},  # 休息 ---common
    "7002": {"name": u'产',        "color": ""       , "id": "7002", "check_type": 0},  # 产假
    "7003": {"name": u'事',        "color": ""       , "id": "7003", "check_type": 0},  # 事假
    "7004": {"name": u'婚',        "color": ""       , "id": "7004", "check_type": 0},  # 婚假
    "7005": {"name": u'丧',        "color": ""       , "id": "7005", "check_type": 0},  # 丧假
    "7006": {"name": u'年',        "color": ""       , "id": "7006", "check_type": 0},  # 年假

    "9999": {"name": u'',        "color": ""       , "id": "9999", "check_type": 0},  # 自定义非上班
}






#####################---ipad---#######################
# namespace: IPAD_
IPAD_SHOP_TYPE_CHOICES = (('A', 'A',), ('B', 'B',), ('C', 'C',))

#####################---end---########################

try:
    from localesettings import *
except:
    pass