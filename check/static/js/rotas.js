

$('#rotas_table').tooltip({
    items: 'td[editable="1"]',
    position: { my: "left top", at: "right center-50%", collision: "flipfit" },
    content: function() {
        // return li;
        var element = $( this );
        var eid = element.attr("data-eid")
        var scheduledate = element.attr("data-scheduledate")
        var weeknum = element.attr("data-weeknum")
        // var text = element.text();
        var html = li.replace(/EID/g, eid);
        html = html.replace(/SCHEDULEDATE/g, scheduledate);
        html = html.replace(/WEEKDAYNUM/g, weeknum);
        //console.log('html: ', html);
        return html;
      },
    show: null, // show immediately
    // open: function(event, ui)
    // {
    //     if (typeof(event.originalEvent) === 'undefined')
    //     {
    //         return false;
    //     }
        
    //     var $id = $(ui.tooltip).attr('id');
        
    //     // close any lingering tooltips
    //     $('div.ui-tooltip').not('#' + $id).remove();
        
    //     // ajax function to pull in data and add it to the tooltip goes here
    // },
    close: function(event, ui)
    {
        ui.tooltip.hover(function()
        {
            $(this).stop(true).fadeTo(400, 1); 
        },
        function()
        {
            $(this).fadeOut('400', function()
            {
                $(this).remove();
            });
        });
    }
});


function myFunction(name, eid, rotaid, scheduledate, weekdaynum)
{
  // alert("Hello World!");
  //  console.log('eid：', eid);
  //  console.log('name：', name);
  // console.log('cell_id：', cell_id);
  // console.log('rotaid：', rotaid);
  // console.log('date_id：', date_id);

  // var x = cell_id.split("_"); 
  // var employ_id = x[0]
  // var weekNum = x[1]
  // console.log('x：', x);

  $.ajax({
        type:'POST',
        url:'/check/rotas/setuprota/',
        data:{ "eid": eid, 'rotaid': rotaid, 'scheduledate': scheduledate, "weekdaynum": weekdaynum},
        cache:false,
        success:function(data){
          if(data.status == 200)
          {
            // $("#"+eid "[weekdaynum='"+weekdaynum+"']").html(name)
              $("[data-eid='"+eid+"'][data-weeknum='"+weekdaynum+"']").html(name);
              $("[data-eid='"+eid+"'][data-weeknum='"+weekdaynum+"']").css("background-color", data.color);
           
            
          }else
          {
            $("#"+eid).html("错误")
          }
          return;
        }
    });

  
};


$( "#datepicker" ).datepicker({

  onSelect: function(date, instance){
    location.href="/check/rotas?date="+date;
   }
});

$("#show-date").click(function () {
  $("#datepicker").datepicker("show");
});


// $( "#report_datepicker" ).datepicker({

//   onSelect: function(date, instance){
//     location.href="/check/report?date="+date;
//    }
// });

$("#show-date-report").click(function () {
  $("#report_datepicker").datepicker("show");
});

