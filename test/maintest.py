# -*- coding: utf-8 -*- 
import urllib2
import logging
import logging.config
import time
import os


TEST_URL = 'http://127.0.0.1:8000/check/getEmployeesInfo/?shop_id=BJ005'
TIME_OUT = 10
CMD = 'D:/test_django/Apache24/bin/httpd -k restart -n "Shop"'

GET_DATA_INTERNAL = 30

# httpd.exe -k restart -n "MyServiceName"
def main():
    logger = logging.getLogger('main')
    try:
        response = urllib2.urlopen(TEST_URL, timeout=TIME_OUT)
        data = response.read()
        logger.info("suc: get date. data len=" + str(len(data)))
    except Exception as e:
        logger.error(e)
        logger.error("try to restart apache: restarting...")
        os.system(CMD)
        logger.error("Restart apache done!")

if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    try:
        from localsettingtest import *
    except:
        pass
    # import pdb; pdb.set_trace()
    while 1:
        main()
        time.sleep(GET_DATA_INTERNAL)
