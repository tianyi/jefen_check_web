# -*- coding: utf-8 -*-

from datetime import datetime
from checkapp.models import MyUser
from django.db import models
from django.core.validators import MaxLengthValidator


def get_file_path(instance, filename):
    # import pdb; pdb.set_trace()
    dd = instance.created_time.__str__()
    dd = dd.replace(":", "-")
    file_name = instance.__class__.__name__ + '/' + dd + '/' + filename
    # print 
    return file_name

#####################----Publish----#######################

class Publish(models.Model):
    user = models.ForeignKey(MyUser)
    name = models.CharField(u'名称', max_length=50)
    desc = models.TextField(u'描述', blank=True)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)
    def __unicode__(self):
        return "Publish--" + self.created_time.__str__()
    class Meta:
        ordering = ['-created_time']

class PublishFile(models.Model):
    publish = models.ForeignKey(Publish, related_name='publish_files')
    file_addr = models.FileField(u'附件', upload_to=get_file_path)
    file_name = models.CharField(u'名称', max_length=100)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now())

######################----End Publish----##################

######################----Meeting--------##################

class Meeting(models.Model):
    # employee_id = models.CharField(u'用户ID', primary_key=True, max_length=100)
    user = models.ForeignKey(MyUser)
    name = models.CharField(u'名称', max_length=50)
    desc = models.TextField(u'描述', blank=True)
    file_addr = models.FileField(u'例会文件', upload_to=get_file_path)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)

    def __unicode__(self):
        # import pdb; pdb.set_trace()
        # x = 1 + 2
        return "Meeting--" + self.created_time.__str__()

    class Meta:
        ordering = ['-created_time']

class MeetingFile(models.Model):
    meeting = models.ForeignKey(Meeting, related_name='meeting_files')
    file_addr = models.FileField(u'附件', upload_to=get_file_path)
    file_name = models.CharField(u'名称', max_length=100)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now())

######################----End Meeting----##################

######################----Training-------##################

class Training(models.Model):
    user = models.ForeignKey(MyUser)
    name = models.CharField(u'名称', max_length=50)
    desc = models.TextField(u'描述', blank=True)
    image = models.ImageField(u'图片', upload_to=get_file_path, max_length=255)
    video_url= models.URLField(u'视频地址', blank=True, max_length=300)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)

    def __unicode__(self):
        return "Training--" + self.created_time.__str__()

    class Meta:
        ordering = ['-created_time']

class TrainingFile(models.Model):
    training = models.ForeignKey(Training, related_name='training_files')
    file_addr = models.FileField(u'附件', upload_to=get_file_path)
    file_name = models.CharField(u'文件名称', max_length=100)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)

##############################################################

class TrainingV1(models.Model):

    user = models.ForeignKey(MyUser)
    name = models.CharField(u'培训名称', max_length=50)
    # image = models.ImageField(u'图片', upload_to=get_file_path, max_length=255)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'培训'
        verbose_name_plural = u'培训'

class BoDuan(models.Model):
    training = models.ForeignKey(TrainingV1, related_name='training_bo_duan')
    bo_duan_name = models.CharField(u'波段名称', max_length=50)
    image = models.ImageField(u'图片', upload_to=get_file_path, max_length=255)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)

    def __unicode__(self):
        return self.bo_duan_name

    class Meta:
        verbose_name = u'波段'
        verbose_name_plural = u'波段'

class BoDuanFile(models.Model):
    bo_duan = models.ForeignKey(BoDuan, related_name='bo_duan_files')
    file_addr = models.FileField(u'附件', upload_to=get_file_path)
    file_name = models.CharField(u'文件名称', max_length=100)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)

    def __unicode__(self):
        return self.bo_duan.bo_duan_name

    class Meta:
        verbose_name = u'波段附件'
        verbose_name_plural = u'波段附件'

######################----End Training----##################

######################----Exhibiting------##################

class Exhibiting(models.Model):
    user = models.ForeignKey(MyUser)
    name = models.CharField(u'名称', max_length=50)
    file_addr = models.FileField(u'附件', upload_to=get_file_path)
    file_name = models.CharField(u'文件名称', max_length=100)
    shop_type = models.CharField(u'店铺类型', max_length=10)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)

    def __unicode__(self):
        return "Training--" + self.created_time.__str__()

    class Meta:
        ordering = ['-created_time']

class ExhibitingImage(models.Model):
    exhibiting = models.ForeignKey(Exhibiting, related_name='exhibiting_images')
    image = models.ImageField(u'图片', upload_to=get_file_path, max_length=255)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now())

######################----End Exhibiting----##################

######################----notifacation------##################

class Notifacation(models.Model):
    user = models.ForeignKey(MyUser)
    name = models.CharField(u'标题', max_length=50)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)
    def __unicode__(self):
        return "Notifacation--" + self.created_time.__str__()

    class Meta:
        ordering = ['-created_time']

class NotifacationFile(models.Model):
    notifacation = models.ForeignKey(Notifacation, related_name='notifacation_files')
    file_addr = models.FileField(u'附件', upload_to=get_file_path)
    file_name = models.CharField(u'名称', max_length=100)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now())

######################----End IpadHelp----##################

class IpadHelp(models.Model):
    user = models.ForeignKey(MyUser)
    file_addr = models.FileField(u'附件', upload_to=get_file_path)
    version = models.CharField(u'版本号', default='0', max_length=10, unique=True)
    created_time = models.DateTimeField(u'上传时间', default=datetime.now)
    
    def __unicode__(self):
        return "IpadHelp--" + self.created_time.__str__()

    class Meta:
        ordering = ['-created_time']

######################----End IpadHelp----##################

