# -*- coding: utf-8 -*- 

# from django.db import models
# from django.db.models.signals import post_save
# from django.forms import ModelForm
from django import forms
# from django.contrib.auth.models import AbstractUser
from django.conf import settings
from datetime import datetime
# from django.forms import ModelForm
from ipad.models import *
from djangoformsetjs.utils import formset_media_js
from common.utils.ExtFileField import ExtFileField


from django import forms

class PublishForm(forms.Form):
    name = forms.CharField(label=u'名称', max_length=50)
    desc = forms.CharField(label=u'描述', max_length=200, widget=forms.Textarea, required=False)
    file_addr = ExtFileField(label=u'文件', required=True, ext_whitelist=['.pdf'])

class PublishFormUpdate(forms.Form):
    name = forms.CharField(label=u'名称', max_length=50)
    desc = forms.CharField(label=u'描述', max_length=200, widget=forms.Textarea, required=False)
    file_addr = ExtFileField(label=u'文件', required=False, ext_whitelist=['.pdf'])

    # def clean_file_addr(self):
    #     import pdb; pdb.set_trace()
    #     value = self.cleaned_data["file"]
    #     print "clean_file value: %s" % value
    #     return None     

class MeetingForm(forms.Form):
    name = forms.CharField(label=u'名称', max_length=50)
    desc = forms.CharField(label=u'描述', max_length=200, widget=forms.Textarea, required=False)
    file_addr = ExtFileField(label=u'文件', required=True, ext_whitelist=['.pdf'])

class MeetingFormUpdate(forms.Form):
    name = forms.CharField(label=u'名称', max_length=50)
    desc = forms.CharField(label=u'描述', max_length=200, widget=forms.Textarea, required=False)
    file_addr = ExtFileField(label=u'文件', required=False, ext_whitelist=['.pdf'])


class TrainingForm(forms.ModelForm):

    desc = forms.CharField(label=u'描述', max_length=200, widget=forms.Textarea, required=False)

    class Meta:
        model = Training
        fields = ['name', 'desc', 'image', 'video_url']


class TrainingFileForm(forms.ModelForm):

    file_addr = ExtFileField(label=u'文件', required=True, ext_whitelist=['.pdf'])

    class Meta:
        model = TrainingFile
        fields = ['file_addr', ]
        

    class Media(object):
        js = formset_media_js + (
            # Other form media here
        )

class ExhibitingForm(forms.ModelForm):

    shop_type = forms.ChoiceField(label=u'店铺类型', widget=forms.RadioSelect, choices=settings.IPAD_SHOP_TYPE_CHOICES)
    file_addr = ExtFileField(label=u'文件', required=True, ext_whitelist=['.pdf'])

    class Meta:
        model = Exhibiting
        fields = ['name', 'file_addr', 'shop_type']

class NotifacationForm(forms.Form):

    name = forms.CharField(label=u'通知名称', max_length=50)
    # file_addr = forms.FileField(label=u'文件', required=True)
    file_addr = ExtFileField(label=u'文件', required=True, ext_whitelist=['.doc', '.docx'])

################# Notifacation ############################
# from django.forms.models import inlineformset_factory

# class NotifacationForm(ModelForm):
#     class Meta:
#         model = Notifacation

# NotifacationFileFormSet = inlineformset_factory(Notifacation, NotifacationFile)
# InstructionFormSet = inlineformset_factory(Recipe, Instruction)

class IpadHelpForm(forms.ModelForm):

    # file_addr = forms.FileField(label=u'文件', required=True)
    file_addr = ExtFileField(label=u'文件', required=True, ext_whitelist=['.pdf'])

    class Meta:
        model = IpadHelp
        fields = ['file_addr', 'version']