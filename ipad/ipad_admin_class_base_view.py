# -*- coding: utf-8 -*- 


# from extra_views import InlineFormSet, CreateWithInlinesView, UpdateWithInlinesView
# from extra_views.generic import GenericInlineFormSet
# from ipad.models import Notifacation, NotifacationFile
# from django.forms import ModelForm

# from django.views.generic import ListView

# class NotifacationListView(ListView):
    
#     model = Notifacation

#     def get_context_data(self, **kwargs):
#         # Call the base implementation first to get a context
#         context = super(NotifacationListView, self).get_context_data(**kwargs)
#         # Add in a QuerySet of all the books

#         # context['book_list'] = Book.objects.all()
#         return context

# class NotifacationForm(ModelForm):
#     class Meta:
#         model = Notifacation

# class NotifacationFileInline(InlineFormSet):
#     model = NotifacationFile


# # class TagsInline(GenericInlineFormSet):
# #     model = Tag


# class NotifacationCreateView(CreateWithInlinesView):

#     template_name = 'ipad/notifacation_add.html'

#     model = Notifacation
#     inlines = [NotifacationFileInline, ]

#     def get_success_url(self):
#         return self.object.get_absolute_url()


# class NotifacationUpdateView(UpdateWithInlinesView):
#     model = Notifacation
#     form_class = NotifacationForm
#     inlines = [NotifacationFileInline, ]

#     def get_success_url(self):
#         return self.object.get_absolute_url()
from django.views.generic import CreateView
from django import forms
from ipad.models import Notifacation, NotifacationFile
from django.forms.models import inlineformset_factory, modelformset_factory
from django.http import HttpResponseRedirect


# class NotifacationForm(forms.Form):

#     name = forms.CharField(label=u'通知名称', max_length=50)
#     file_addr = forms.FileField(label=u'文件', required=True)
    # class Meta:
    #     model = Notifacation
    #     fields=('name', )

# NotifacationFileFormSet = modelformset_factory(
#             NotifacationFile,
#             fields=('file_addr', ),
#             extra=1,
#         )

# TrainingFileFormSet = inlineformset_factory(
#         Training, 
#         TrainingFile,
#         fields=('file_addr', ),
#         extra=0,
#         widgets={'desc': Textarea(attrs={'cols': 80, 'rows': 10}), },
#     )
# InstructionFormSet = inlineformset_factory(Recipe, Instruction)

# class NotifacationCreateView(CreateView):

    
#     model = Notifacation
#     form_class = NotifacationForm
#     success_url = '/ipad/admin/notifacation/'

#     def get(self, request, *args, **kwargs):
#         """
#         Handles GET requests and instantiates blank versions of the form
#         and its inline formsets.
#         """
#         self.object = None
#         import pdb; pdb.set_trace()
#         form_class = self.get_form_class()
#         form = self.get_form(form_class)
#         # notifacationfile_form = NotifacationFileFormSet(queryset=NotifacationFile.objects.none())
#         # instruction_form = InstructionFormSet()
#         return self.render_to_response(self.get_context_data(notifacation_form=form))

    # def post(self, request, *args, **kwargs):
    #     """
    #     Handles POST requests, instantiating a form instance and its inline
    #     formsets with the passed POST variables and then checking them for
    #     validity.
    #     """
    #     self.object = None
    #     form_class = self.get_form_class()
    #     form = self.get_form(form_class)


    #     # import pdb; pdb.set_trace()
    #     notifacationfile_form = NotifacationFileFormSet(
    #             self.request.POST, 
    #             self.request.FILES
    #         )
    #     # import pdb; pdb.set_trace()
    #     if (form.is_valid() and notifacationfile_form.is_valid()):
    #         return self.form_valid(form, notifacationfile_form)
    #     else:
    #         return self.form_invalid(form, notifacationfile_form)

    # def form_valid(self, form, notifacationfile_form):
    #     """
    #     Called if all forms are valid. Creates a Recipe instance along with
    #     associated Ingredients and Instructions and then redirects to a
    #     success page.
    #     """

    #     instance_model = form.save(commit=False)
    #     instance_model.user = self.request.user
    #     instance_model.save()

    #     instance_models = notifacationfile_form.save(commit=False)

    #     for ins in instance_models:
    #         ins.notifacation = instance_model
    #         ins.file_name = ins.file_addr.name
    #         ins.save()
    #     # url = self.get_success_url()
    #     # import pdb; pdb.set_trace()
    #     return HttpResponseRedirect(self.success_url)

    # def form_invalid(self, form, notifacationfile_form):
    #     """
    #     Called if a form is invalid. Re-renders the context data with the
    #     data-filled forms and errors.
    #     """
    #     return self.render_to_response(
    #         self.get_context_data(notifacation_form=form,
    #                               formset=notifacationfile_form))

