# -*- coding: utf-8 -*- 

# from django.contrib.auth.models import Group
# from django.contrib.auth import get_user_model
# from django.views.generic import CreateView

# User = get_user_model()

# class CreateView

# views.py
from django.views.generic import ListView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from ipad.models import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from ipad_admin_form import *
from datetime import datetime
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
# from django.forms.formsets import formset_factory
from django.forms.models import modelformset_factory
from django.forms import Textarea, HiddenInput
from django.forms.models import inlineformset_factory
import os
# from django.views.generic import CreateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from common.utils.ExtFileField import ExtFileField

# class PublishList(ListView):
#     model = Publish
@login_required
def publish_list(request):
    pub = Publish.objects.all()
    object_list = []
    for pp in pub:
        pf = PublishFile.objects.filter(publish=pp)
        object_list.append({'publish': pp, 'publish_file': pf})
    # print len(object_list)
    # print object_list
    return render_to_response(
        'ipad/publish_list.html',
        {'object_list': object_list},
        context_instance=RequestContext(request),
    )

@login_required
def publish_delete(request, pk):
    Publish.objects.get(id=pk).delete()
    # PublishFile.objects.filter()
    return HttpResponseRedirect(reverse('publish_list'))

@login_required
def publish_update(request, pk):

    # import pdb; pdb.set_trace()
    pub = Publish.objects.get(id=pk)
    pf = PublishFile.objects.get(publish=pub)
    if request.method == "POST":
        publish_form = PublishFormUpdate(request.POST, request.FILES)
        if publish_form.is_valid():
            # if publish_form.cleaned_data['file_addr']:
            #     pf.file_addr = publish_form.cleaned_data['file_addr']

            pf.created_time = datetime.now()
            pf.save()
            pub.name = publish_form.cleaned_data['name']
            pub.desc = publish_form.cleaned_data['desc']
            # do not update created time
            pub.created_time = datetime.now()
            pub.save()
            return HttpResponseRedirect(reverse('publish_list'))

    elif request.method == "GET":
        
        initial = {
                'name': pub.name,
                'desc': pub.desc,
                'file_addr': pf.file_addr,
            }
        publish_form = PublishFormUpdate(initial=initial)

    return render_to_response(
        'ipad/publish_update.html',
        {'publish_form': publish_form},
        context_instance=RequestContext(request),
    )

@login_required
def publish_add(request):

    if request.method == "POST":
        publish_form = PublishForm(request.POST, request.FILES)
        if publish_form.is_valid():
            # import pdb; pdb.set_trace()
            file_addr=publish_form.cleaned_data['file_addr']
            file_name=file_addr.name

            pub = Publish(
                    user=request.user,
                    name=publish_form.cleaned_data['name'],
                    desc=publish_form.cleaned_data['desc'],
                    created_time=datetime.now(),
                )
            pub.save()
            PublishFile(
                    publish=pub,
                    file_addr=file_addr,
                    file_name=file_name,
                    created_time=datetime.now(),
                ).save()

            return HttpResponseRedirect(reverse('publish_list'))

    elif request.method == "GET":
        publish_form = PublishForm()

    return render_to_response(
        'ipad/publish_add.html',
        {'publish_form': publish_form},
        context_instance=RequestContext(request),
    )





@login_required
def meeting_list(request):

    pub = Meeting.objects.all()
    object_list = []
    for pp in pub:
        pf = MeetingFile.objects.filter(meeting=pp)
        object_list.append({'meeting': pp, 'meeting_file': pf})
    # print len(object_list)
    # print object_list
    return render_to_response(
        'ipad/meeting_list.html',
        {'object_list': object_list},
        context_instance=RequestContext(request),
    )


@login_required
def meeting_update(request, pk):

    met = Meeting.objects.get(id=pk)
    mf = MeetingFile.objects.get(meeting=met)
    if request.method == "POST":
        meeting_form = MeetingFormUpdate(request.POST, request.FILES)
        if meeting_form.is_valid():
            if meeting_form.cleaned_data['file_addr']:
                mf.file_addr = meeting_form.cleaned_data['file_addr']

            mf.created_time = datetime.now()
            mf.save()
            met.name = meeting_form.cleaned_data['name']
            met.desc = meeting_form.cleaned_data['desc']
            # do not update created time
            met.created_time = datetime.now()
            met.save()
            return HttpResponseRedirect(reverse('meeting_list'))

    elif request.method == "GET":
        
        initial = {
                'name': met.name,
                'desc': met.desc,
                'file_addr': mf.file_addr,
            }
        meeting_form = MeetingFormUpdate(initial=initial)

    return render_to_response(
        'ipad/meeting_update.html',
        {'meeting_form': meeting_form},
        context_instance=RequestContext(request),
    )


@login_required
def meeting_add(request):

    if request.method == "POST":
        meeting_form = MeetingForm(request.POST, request.FILES)
        if meeting_form.is_valid():
            # import pdb; pdb.set_trace()
            file_addr=meeting_form.cleaned_data['file_addr']
            file_name=file_addr.name

            met = Meeting(
                    user=request.user,
                    name=meeting_form.cleaned_data['name'],
                    desc=meeting_form.cleaned_data['desc'],
                    created_time=datetime.now(),
                )
            met.save()
            MeetingFile(
                    meeting=met,
                    file_addr=file_addr,
                    file_name=file_name,
                    created_time=datetime.now(),
                ).save()

            return HttpResponseRedirect(reverse('meeting_list'))

    elif request.method == "GET":
        meeting_form = MeetingForm()

    return render_to_response(
        'ipad/meeting_add.html',
        {'meeting_form': meeting_form},
        context_instance=RequestContext(request),
    )

@login_required
def meeting_delete(request, pk):
    Meeting.objects.get(id=pk).delete()
    return HttpResponseRedirect(reverse('meeting_list'))

@login_required
def training_list(request):
    tra = Training.objects.all()
    object_list = []
    for tr in tra:
        tf = TrainingFile.objects.filter(training=tr)
        initial=[]
        for obj in tf:
            initial.append({
                'training': obj.training, 
                'file_addr': obj.file_addr, 
                'file_name': obj.file_name,
            })

        object_list.append({
            'training': 
                {
                    'id': tr.id,
                    'name': tr.name,
                    'desc': tr.desc,
                    'image': tr.image,
                    'video_url': tr.video_url,
                    'created_time': tr.created_time,
                }, 
            'training_file': initial,
        })
    # import pdb; pdb.set_trace()
    return render_to_response(
        'ipad/training_list.html',
        {'object_list': object_list},
        context_instance=RequestContext(request),
    )

@login_required
def training_delete(request, pk):
    Training.objects.get(id=pk).delete()
    return HttpResponseRedirect(reverse('training_list'))

@login_required
def training_add(request):
    # import pdb; pdb.set_trace()
    TrainingFileFormSet =  modelformset_factory(
        TrainingFile, 
        form=TrainingFileForm,
        
    )

    if request.method == "POST":
        # import pdb; pdb.set_trace()
        training_form = TrainingForm(request.POST, request.FILES)
        trainingfile_formset = TrainingFileFormSet(request.POST, request.FILES)
        if training_form.is_valid():
            new_training = training_form.save(commit=False)
            new_training.user = request.user
            new_training.save()

            instances = trainingfile_formset.save(commit=False)
            if trainingfile_formset.is_valid():
                for instance in instances:
                    # import pdb; pdb.set_trace()
                    instance.file_name = instance.file_addr.name
                    instance.training = new_training
                    instance.save()
                return HttpResponseRedirect(reverse('training_list'))

    elif request.method == "GET":
        training_form = TrainingForm()
        # import pdb; pdb.set_trace()
        trainingfile_formset = TrainingFileFormSet(queryset=TrainingFile.objects.none())
    return render_to_response(
        'ipad/training_add.html',
        {
            'training_form': training_form, 
            'formset': trainingfile_formset
        },
        context_instance=RequestContext(request),
    )


@login_required
def training_update(request, pk):

    training = Training.objects.get(id=pk)
    TrainingFileFormSet = inlineformset_factory(
        Training, 
        TrainingFile,
        form=TrainingFileForm,
        fields=('file_addr', ),
        extra=0,
        # widgets={
        #     'desc': Textarea(), 
        #     # 'file_addr': ExtFileField(label=u'文件', required=True, ext_whitelist=['.pdf']),
        # },
    )
    if request.method == "POST":
        # import pdb; pdb.set_trace()
        training_form = TrainingForm(request.POST, request.FILES, instance=training)
        trainingfile_formset = TrainingFileFormSet(request.POST, request.FILES, instance=training)
        if training_form.is_valid():
            training_form.save()
        if trainingfile_formset.is_valid():
            # import pdb; pdb.set_trace()
            trainingfile_formset.save()
            qs = TrainingFile.objects.filter(training_id=pk)
            for obj in qs:
                obj.file_name = os.path.basename(obj.file_addr.name)
                obj.save()
            return HttpResponseRedirect(reverse('training_list'))
    elif request.method == "GET":
        
        # import pdb; pdb.set_trace()
        training_form = TrainingForm(instance=training)
        trainingfile_formset = TrainingFileFormSet(instance=training)
    return render_to_response(
        'ipad/training_add.html',
        {
            'training_form': training_form, 
            'formset': trainingfile_formset
        },
        context_instance=RequestContext(request),
    )

@login_required
def exhibiting_list(request):
    tra = Exhibiting.objects.all()
    object_list = []
    for tr in tra:
        tf = ExhibitingImage.objects.filter(exhibiting=tr)
        initial=[]
        for obj in tf:
            initial.append({
                'exhibiting': obj.exhibiting, 
                'image': obj.image, 
            })

        object_list.append({
            'exhibiting': {
                    'id': tr.id,
                    'name': tr.name,
                    'file_addr': tr.file_addr,
                    'file_name': tr.file_name,
                    'shop_type': tr.shop_type,
                    'created_time': tr.created_time,
                }, 
            'exhibiting_file': initial,
        })
    # import pdb; pdb.set_trace()
    return render_to_response(
        'ipad/exhibiting_list.html',
        {'object_list': object_list},
        context_instance=RequestContext(request),
    )

@login_required
def exhibiting_delete(request, pk):
    Exhibiting.objects.get(id=pk).delete()
    return HttpResponseRedirect(reverse('exhibiting_list'))

@login_required
def exhibiting_add(request):
    # import pdb; pdb.set_trace()
    ExhibitingImageFormSet =  modelformset_factory(
        ExhibitingImage, fields=('image',)
        )

    if request.method == "POST":
        # import pdb; pdb.set_trace()
        exhibiting_form = ExhibitingForm(request.POST, request.FILES)
        exhibiingimage_formset = ExhibitingImageFormSet(request.POST, request.FILES)
        if exhibiting_form.is_valid():
            new_exhibiting = exhibiting_form.save(commit=False)
            new_exhibiting.user = request.user
            new_exhibiting.file_name = new_exhibiting.file_addr.name
            new_exhibiting.save()
            if exhibiingimage_formset.is_valid():
                instances = exhibiingimage_formset.save(commit=False)
                for instance in instances:
                    instance.exhibiting = new_exhibiting
                    instance.save()
                return HttpResponseRedirect(reverse('exhibiting_list'))

    elif request.method == "GET":
        exhibiting_form = ExhibitingForm()
        # import pdb; pdb.set_trace()
        exhibiingimage_formset = ExhibitingImageFormSet(queryset=ExhibitingImage.objects.none())
    return render_to_response(
        'ipad/exhibiting_add.html',
        {
            'exhibiting_form': exhibiting_form, 
            'formset': exhibiingimage_formset
        },
        context_instance=RequestContext(request),
    )

@login_required
def exhibiting_update(request, pk):

    exhibiting = Exhibiting.objects.get(id=pk)
    ExhtbitingImageFormSet = inlineformset_factory(
        Exhibiting, 
        ExhibitingImage,
        fields=('image', ),
        extra=0,
    )

    if request.method == "POST":
        exhibiting_form = ExhibitingForm(request.POST, request.FILES, instance=exhibiting)
        exhibitingimage_formset = ExhtbitingImageFormSet(request.POST, request.FILES, instance=exhibiting)
        if exhibiting_form.is_valid():
            new_exhibiting = exhibiting_form.save(commit=False)
            new_exhibiting.file_name = new_exhibiting.file_addr.name
            exhibiting_form.save()
        
        if exhibitingimage_formset.is_valid():
            exhibitingimage_formset.save()
            return HttpResponseRedirect(reverse('exhibiting_list'))
    elif request.method == "GET":
        exhibiting_form = ExhibitingForm(instance=exhibiting)
        exhibitingimage_formset = ExhtbitingImageFormSet(instance=exhibiting)
        # import pdb; pdb.set_trace()
    return render_to_response(
        'ipad/exhibiting_add.html',
        {
            'exhibiting_form': exhibiting_form, 
            'formset': exhibitingimage_formset
        },
        context_instance=RequestContext(request),
    )

@login_required
def notifacation_list(request):
    noti = Notifacation.objects.all()

    object_list = []
    for no in noti:
        nf = NotifacationFile.objects.filter(notifacation=no)
        initial=[]
        for obj in nf:
            initial.append({
                'notifacation': obj.notifacation, 
                'file_addr': obj.file_addr, 
                'file_name': obj.file_name,
            })

        object_list.append({
            'notifacation': 
                {
                    'id': no.id,
                    'name': no.name,
                    'created_time': no.created_time,
                }, 
            'notifacation_file': initial,
        })
    # print object_list
    # import pdb; pdb.set_trace()
    return render_to_response(
        'ipad/notifacation_list.html',
        {'object_list': object_list},
        context_instance=RequestContext(request),
    )

@login_required
def notifacation_delete(request, pk):
    Notifacation.objects.get(id=pk).delete()
    return HttpResponseRedirect(reverse('notifacation_list'))

@login_required
def notifacation_add(request):

    if request.method == "POST":
        notifacation_form = NotifacationForm(request.POST, request.FILES)
        if notifacation_form.is_valid():
            file_addr=notifacation_form.cleaned_data['file_addr']
            file_name=file_addr.name
            notifacation = Notifacation(
                    name=notifacation_form.cleaned_data['name'],
                    user=request.user,
            )
            notifacation.save()
            NotifacationFile(
                notifacation=notifacation,
                file_addr=notifacation_form.cleaned_data['file_addr'],
                file_name=notifacation_form.cleaned_data['file_addr'].name,
            ).save()

            return HttpResponseRedirect(reverse('notifacation_list'))

    elif request.method == "GET":
        notifacation_form = NotifacationForm()

    return render_to_response(
        'ipad/notifacation_add.html',
        {'notifacation_form': notifacation_form},
        context_instance=RequestContext(request),
    )


@login_required
def notifacation_update(request, pk):

    noti = Notifacation.objects.get(id=pk)
    nf = NotifacationFile.objects.get(notifacation=noti)
    if request.method == "POST":
        notifacation_form = NotifacationForm(request.POST, request.FILES)
        if notifacation_form.is_valid():
            noti.name = notifacation_form.cleaned_data['name']
            noti.save()
            nf.file_addr = notifacation_form.cleaned_data['file_addr']
            nf.file_name = notifacation_form.cleaned_data['file_addr'].name
            nf.save()
            return HttpResponseRedirect(reverse('notifacation_list'))

    elif request.method == "GET":
        
        initial = {
                'name': noti.name,
                'file_addr': nf.file_addr,
            }
        notifacation_form = NotifacationForm(initial=initial)

    return render_to_response(
        'ipad/notifacation_add.html',
        {'notifacation_form': notifacation_form},
        context_instance=RequestContext(request),
    )


class IpadHelpList(ListView):
    model = IpadHelp


class IpadHelpUpdate(UpdateView):

    # import pdb; pdb.set_trace()
    model = IpadHelp
    fields = ['file_addr', 'version']
    success_url = "/ipad/admin/help/"

class IpadHelpDelete(DeleteView):
    model = IpadHelp
    success_url = reverse_lazy('help_list')

class IpadHelpAdd(CreateView):
    model = IpadHelp

    form_class = IpadHelpForm
    fields = ['file_addr', 'version']
    widgets = {
        'file_addr': ExtFileField(label=u'文件', required=True, ext_whitelist=['.pdf']),
    }
    success_url = reverse_lazy('help_list')
    # success_url = "/ipad/admin/help_list/"

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()     
        self.object = obj   
        # import pdb; pdb.set_trace()
        return HttpResponseRedirect(self.get_success_url())
        # self.render_to_response(self.get_context_data(form=form))
