from rest_framework.renderers import JSONRenderer
from django.conf import settings

class MyJSONRenderer(JSONRenderer):

    # set accepted_media_type charset to client
    charset = "utf-8"
    """
        Override the render method of the django rest framework JSONRenderer to allow the following:
        * adding a resource_name root element to all GET requests formatted with JSON
        * reformatting paginated results to the following structure {meta: {}, resource_name: [{},{}]}

        NB: This solution requires a custom pagination serializer and an attribute of 'resource_name'
            defined in the serializer
    """
    def render(self, data, accepted_media_type=None, renderer_context=None):
        response_data = {}

        # import pdb; pdb.set_trace()

        #determine the resource name for this request - default to objects if not defined
        try:
            resource = getattr(renderer_context.get('view').get_serializer().Meta, 'resource_name', 'data')
        except:
            resource = settings.REST_FRAMEWORK['DEFAULT_API_ROOT']

        #check if the results have been paginated
        if data.get('paginated_results'):
            #add the resource key and copy the results
            response_data['meta'] = data.get('meta')
            response_data[resource] = data.get('paginated_results')
        else:
            response_data[resource] = data

        # add error code and error message
        response_data.update({'errcode': '0', 'errmsg': 'ok'})

        #call super to render the response
        # import pdb; pdb.set_trace()

        # add utf-8: application/json;charset=UTF-8
        # if()

        response = super(MyJSONRenderer, self).render(response_data, accepted_media_type, renderer_context)

        return response