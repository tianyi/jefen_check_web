# -*- coding: utf-8 -*-

from django.contrib.auth.models import Group
from rest_framework import viewsets
from ipad.serializers import *
from django.contrib.auth import get_user_model
from ipad.models import *
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from dto import UpdateTimeModel
from rest_framework.decorators import api_view
import datetime
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from django.shortcuts import render_to_response
from django.template import RequestContext


User = get_user_model()

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

class PublishViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Publish.objects.all()
    serializer_class = PublishSerializer

class PublishViewSetV1(viewsets.ReadOnlyModelViewSet):

    queryset = Publish.objects.all()
    serializer_class = PublishSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Publish.objects.all()
        # import pdb; pdb.set_trace()
        date1 = self.request.query_params.get('date', None)

        try:
            date1 = datetime.datetime.strptime(date1, settings.REST_FRAMEWORK["FORMAT_DATE"])
        except:
            date1 = None
        # print "shop_type=" + str(shop_type)
        if date1 is not None:
            start = datetime.datetime.combine(date1, datetime.time.min)
            end = datetime.datetime.combine(date1, datetime.time.max)
            queryset = queryset.filter(created_time__range=(start, end))
        # else:
        #     queryset = None
        return queryset

class MeetingViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Meeting.objects.all()
    serializer_class = MeetingSerializer

class MeetingViewSetV1(viewsets.ReadOnlyModelViewSet):

    queryset = Meeting.objects.all()
    serializer_class = MeetingSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Meeting.objects.all()
        # import pdb; pdb.set_trace()
        date1 = self.request.query_params.get('date', None)

        try:
            date1 = datetime.datetime.strptime(date1, settings.REST_FRAMEWORK["FORMAT_DATE"])
        except:
            date1 = None
        # print "shop_type=" + str(shop_type)
        if date1 is not None:
            start = datetime.datetime.combine(date1, datetime.time.min)
            end = datetime.datetime.combine(date1, datetime.time.max)
            queryset = queryset.filter(created_time__range=(start, end))
        # else:
        #     queryset = None
        return queryset

class MeetingAvaiableDate(viewsets.ViewSet):

    '''
        meeting_date: 返回例会有数据的日期
    '''

    def list(self, request, format=None):

        meeting = [vv['created_time'].strftime(settings.REST_FRAMEWORK["FORMAT_DATE"]) for vv in Meeting.objects.values('created_time')]
        return Response({
            "meeting_date": meeting,
        })

class NotificationAvaiableDate(viewsets.ViewSet):

    def list(self, request, format=None):

        notifacation = [vv['created_time'].strftime(settings.REST_FRAMEWORK["FORMAT_DATE"]) for vv in Notifacation.objects.values('created_time')]
        return Response({
            "notifacation_date": notifacation,
        })

class PublishAvaiableDate(viewsets.ViewSet):

    def list(self, request, format=None):

        publish = [vv['created_time'].strftime(settings.REST_FRAMEWORK["FORMAT_DATE"]) for vv in Publish.objects.values('created_time')]
        return Response({
            "publish_date": publish,
        })

class ExhibitingViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Exhibiting.objects.all()
    serializer_class = ExhibitingSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Exhibiting.objects.all()
        # import pdb; pdb.set_trace()
        shop_type = self.request.query_params.get('shop_type', None)
        # print "shop_type=" + str(shop_type)
        if shop_type is not None and len(shop_type)!=0:
            queryset = queryset.filter(shop_type=shop_type)
        return queryset

class TrainingViewSet(viewsets.ReadOnlyModelViewSet):
    
    queryset = Training.objects.all()
    serializer_class = TrainingSerializer

class TrainingViewSetV1(viewsets.ReadOnlyModelViewSet):
    
    queryset = TrainingV1.objects.all()
    serializer_class = TrainingSerializerV1


class NotifacationViewSet(viewsets.ReadOnlyModelViewSet):
    
    queryset = Notifacation.objects.all()
    serializer_class = NotifacationSerializer

class NotifacationViewSetV1(viewsets.ReadOnlyModelViewSet):
    
    queryset = Notifacation.objects.all()
    serializer_class = NotifacationSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Notifacation.objects.all()
        # import pdb; pdb.set_trace()
        date1 = self.request.query_params.get('date', None)

        try:
            date1 = datetime.datetime.strptime(date1, settings.REST_FRAMEWORK["FORMAT_DATE"])
        except:
            date1 = None
        # print "shop_type=" + str(shop_type)
        if date1 is not None:
            start = datetime.datetime.combine(date1, datetime.time.min)
            end = datetime.datetime.combine(date1, datetime.time.max)
            queryset = queryset.filter(created_time__range=(start, end))
        # else:
        #     queryset = None
        return queryset

class IpadHelpViewSet(viewsets.ReadOnlyModelViewSet):
    
    queryset = IpadHelp.objects.all()
    serializer_class = IpadHelpSerializer




# def file_to_client(request):

#     my_data = open("d:/Desktop/tmp/1.pdf", 'rb')

#     response = HttpResponse(my_data, content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename="foo.xls"'

#     return response


def terms_of_service(request):
    
    return render_to_response(
        'ipad/ipad_termsofservice.html',
        context_instance=RequestContext(request),
    )


class UpdateTimeViewSet(viewsets.ViewSet):

    def list(self, request, format=None):

        default_date = datetime.datetime(2015, 1, 1)

        try:
            latest_publish = Publish.objects.latest('created_time').created_time
        except:
            latest_publish = default_date
        try:
            latest_meeting = Meeting.objects.latest('created_time').created_time
        except:
            latest_meeting = default_date
        try:
            latest_training = Training.objects.latest('created_time').created_time
        except:
            latest_training = default_date
        try:
            latest_exhibiting = Exhibiting.objects.latest('created_time').created_time
        except:
            latest_exhibiting = default_date


        return Response({
            "publish_updatetime": latest_publish.strftime(settings.REST_FRAMEWORK['FORMAT_DATATIME']),
            "meeting_updatetime": latest_meeting.strftime(settings.REST_FRAMEWORK['FORMAT_DATATIME']),
            "training_updatetime": latest_training.strftime(settings.REST_FRAMEWORK['FORMAT_DATATIME']),
            "exhibiting_updatetime": latest_exhibiting.strftime(settings.REST_FRAMEWORK['FORMAT_DATATIME']),
        })


