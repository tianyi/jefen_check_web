# from django.contrib.auth.models import Group
from ipad.models import *
from rest_framework import serializers
from rest_framework import permissions
from django.conf import settings


from django.contrib.auth import get_user_model

User = get_user_model()





class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        resource_name = settings.REST_FRAMEWORK['DEFAULT_API_ROOT']
        fields = ('url', 'username', 'shop_name', 'shop_id')

class PublishFileSerializer(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])

    class Meta:
        model = PublishFile
        fields = ('file_addr', 'file_name', 'created_time')

class MeetingFileSerializer(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])

    class Meta:
        model = MeetingFile
        fields = ('file_addr', 'file_name', 'created_time')

class PublishSerializer(serializers.ModelSerializer):

    # import pdb; pdb.set_trace()

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    publish_files = PublishFileSerializer(many=True)  # A nested list of 'file' items.

    class Meta:
        model = Publish
        resource_name = settings.REST_FRAMEWORK['DEFAULT_API_ROOT']
        fields = ('id', 'name', 'desc', 'user', 'created_time', 'publish_files')


class MeetingSerializer(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    meeting_files = MeetingFileSerializer(many=True)  # A nested list of 'file' items.

    class Meta:
        model = Meeting
        resource_name = settings.REST_FRAMEWORK['DEFAULT_API_ROOT']
        fields = ('id', 'name', 'desc', 'user', 'created_time', 'meeting_files')

######################----Training------##################

class TrainingFileSerializer(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])

    class Meta:
        model = TrainingFile
        fields = ('file_addr', 'file_name', 'created_time')

class TrainingSerializer(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    training_files = TrainingFileSerializer(many=True)  # A nested list of 'file' items.

    class Meta:
        model = Training
        resource_name = settings.REST_FRAMEWORK['DEFAULT_API_ROOT']
        fields = ('id', 'name', 'desc', 'image', 'video_url', 'user', 'created_time', 'training_files')

######################----End Training----##################

######################----new Training------##################

class BoDuanFileSerializerV1(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])

    class Meta:
        model = BoDuanFile
        fields = ('file_addr', 'file_name', 'created_time')

class BoDuanSerializer(serializers.ModelSerializer):
    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    bo_duan_files = BoDuanFileSerializerV1(many=True)  # A nested list of 'file' items.
    
    class Meta:
        model = BoDuan
        fields = ('bo_duan_name', 'image', 'bo_duan_files', 'created_time')

class TrainingSerializerV1(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    training_bo_duan = BoDuanSerializer(many=True)  # A nested list of 'file' items.

    class Meta:
        model = TrainingV1
        resource_name = settings.REST_FRAMEWORK['DEFAULT_API_ROOT']
        fields = ('id', 'name', 'user', 'training_bo_duan', 'created_time')

######################----End new Training----##################

######################----Exhibiting------##################

class ExhibitingImageSerializer(serializers.ModelSerializer):
    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    class Meta:
        model = ExhibitingImage
        fields = ('image', 'created_time')

class ExhibitingSerializer(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    exhibiting_images = ExhibitingImageSerializer(many=True)

    class Meta:
        model = Exhibiting
        resource_name = settings.REST_FRAMEWORK['DEFAULT_API_ROOT']
        fields = ('id', 'name', 'file_name', 'file_addr', 'shop_type', 'user', 'created_time', 'exhibiting_images')

######################----End Exhibiting----##################

class UpdateTimeSerializer(serializers.Serializer):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    publish_updatetime = serializers.DateTimeField(
        format=settings.REST_FRAMEWORK['FORMAT_DATATIME'], read_only=True, source='publish_update')
    meeting_updatetime = serializers.DateTimeField(
        format=settings.REST_FRAMEWORK['FORMAT_DATATIME'], read_only=True, source='meeting_update')
    training_updatetime = serializers.DateTimeField(
        format=settings.REST_FRAMEWORK['FORMAT_DATATIME'], read_only=True, source='training_update')
    exhibiting_updatetime = serializers.DateTimeField(
        format=settings.REST_FRAMEWORK['FORMAT_DATATIME'], read_only=True, source='exhibiting_update')


######################----notifacation------##################

class NotifacationFileSerializer(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])

    class Meta:
        model = MeetingFile
        fields = ('file_addr', 'file_name', 'created_time')

class NotifacationSerializer(serializers.ModelSerializer):

    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    notifacation_files = NotifacationFileSerializer(many=True) 
    
    class Meta:
        model = Notifacation
        resource_name = settings.REST_FRAMEWORK['DEFAULT_API_ROOT']
        fields = ('id', 'name', 'created_time', 'notifacation_files')

######################----End notifacation----##################


######################----notifacation------##################

class IpadHelpSerializer(serializers.ModelSerializer):
    created_time = serializers.DateTimeField(format=settings.REST_FRAMEWORK['FORMAT_DATATIME'])
    class Meta:
        model = IpadHelp
        fields = ('file_addr', 'version', 'created_time')

######################----End notifacation----##################
