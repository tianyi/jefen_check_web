from django.conf import settings
from django.conf.urls import patterns, url, include
from rest_framework.authtoken import views as token_view
# import views_account
from rest_framework import routers
from ipad import views
from ipad import admin_view


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'publish', views.PublishViewSet)
router.register(r'publish_v1', views.PublishViewSetV1, base_name="publish_v1")
router.register(r'meeting', views.MeetingViewSet)
router.register(r'meeting_v1', views.MeetingViewSetV1, base_name="meeting_v1")
router.register(r'exhibiting', views.ExhibitingViewSet)
router.register(r'training', views.TrainingViewSet)
router.register(r'training_v1', views.TrainingViewSetV1)
router.register(r'notifacation', views.NotifacationViewSet)
router.register(r'notifacation_v1', views.NotifacationViewSetV1, base_name="notifacation_v1")
router.register(r'help', views.IpadHelpViewSet)
router.register(r'updatetime', views.UpdateTimeViewSet, base_name="updatetime")
router.register(r'meeting_date', views.MeetingAvaiableDate, base_name="meeting_date")
router.register(r'notification_date', views.NotificationAvaiableDate, base_name="notification_date")
router.register(r'publish_date', views.PublishAvaiableDate, base_name="publish_date")

urlpatterns = patterns(
    "ipad.views",
    url(r'^api-token-auth/', token_view.obtain_auth_token),
    url(r'^api/', include(router.urls)),
    # url(r'^api/updatetime/$', views.update_time, name='updatetime'),
    # url(r'^api/getupdatetime/$', views.GetUpdateTime.as_view(), name='getupdatetime'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # 
    url(r'^api/terms_of_service/$', views.terms_of_service, name='terms_of_service'),

    # admin
    # publish
    url(r'^admin/publish/$', admin_view.publish_list, name="publish_list"),
    url(r'^admin/publish/(?P<pk>\d+)/$', admin_view.publish_update),
    url(r'^admin/publish/(?P<pk>\d+)/delete/$', admin_view.publish_delete),
    url(r'^admin/publish/add/$', admin_view.publish_add),

    # meeting
    url(r'^admin/meeting/$', admin_view.meeting_list, name="meeting_list"),
    url(r'^admin/meeting/(?P<pk>\d+)/$', admin_view.meeting_update),
    url(r'^admin/meeting/(?P<pk>\d+)/delete/$', admin_view.meeting_delete),
    url(r'^admin/meeting/add/$', admin_view.meeting_add),

    # training
    url(r'^admin/training/$', admin_view.training_list, name="training_list"),
    url(r'^admin/training/(?P<pk>\d+)/$', admin_view.training_update),
    url(r'^admin/training/(?P<pk>\d+)/delete/$', admin_view.training_delete),
    url(r'^admin/training/add/$', admin_view.training_add),

    # exhibition
    url(r'^admin/exhibiting/$', admin_view.exhibiting_list, name="exhibiting_list"),
    url(r'^admin/exhibiting/(?P<pk>\d+)/$', admin_view.exhibiting_update),
    url(r'^admin/exhibiting/(?P<pk>\d+)/delete/$', admin_view.exhibiting_delete),
    url(r'^admin/exhibiting/add/$', admin_view.exhibiting_add),

    # noticafacion
    url(r'^admin/notification/$', admin_view.notifacation_list, name='notifacation_list'),
    url(r'^admin/notification/(?P<pk>\d+)/$', admin_view.notifacation_update),
    url(r'^admin/notification/add/$', admin_view.notifacation_add, name='notifacation_add'),
    url(r'^admin/notification/(?P<pk>\d+)/delete/$',  admin_view.notifacation_delete, name='notifacation_delete'),

    # help
    # 
    url(r'^admin/help/$', admin_view.IpadHelpList.as_view(template_name="ipad/ipad_help_list.html"), name='help_list'),
    url(r'admin/help/(?P<pk>\d+)/$', admin_view.IpadHelpUpdate.as_view(template_name="ipad/ipad_help_update.html"), name='help_update'),
    url(r'admin/help/delete/(?P<pk>\d+)/$', admin_view.IpadHelpDelete.as_view(template_name="ipad/ipad_help_delete.html"), name='help_delete'),
    url(r'admin/help/add/$', admin_view.IpadHelpAdd.as_view(template_name="ipad/ipad_help_add.html"), name='help_add'),

    # file to client
    # url(r'^file_to_client/$', views.file_to_client, name='file_to_client'),

    # backend admin
    # url(r'^publish/$', 'publish', name='publish'),
)