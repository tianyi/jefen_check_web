# -*- coding: utf-8 -*- 

from django.contrib import admin
from ipad.models import *
from nested_inline.admin import NestedStackedInline, NestedModelAdmin, NestedTabularInline
from common.utils.ExtFileField import ExtFileField
from django import forms
# Register your models here.

admin.site.register(Publish)
admin.site.register(PublishFile)
admin.site.register(Meeting)
admin.site.register(MeetingFile)
admin.site.register(Training)
admin.site.register(TrainingFile)
admin.site.register(Exhibiting)
admin.site.register(ExhibitingImage)
admin.site.register(Notifacation)
admin.site.register(NotifacationFile)
admin.site.register(IpadHelp)

# admin.site.register(TrainingV1)
# admin.site.register(BoDuan)
# admin.site.register(BoDuanFile)

# class BoDuanFileInline(admin.TabularInline):
#     model = BoDuanFile
class BoDuanFileForm(forms.ModelForm):

    # desc = forms.CharField(label=u'描述', max_length=200, widget=forms.Textarea, required=False)
    file_addr = ExtFileField(label=u'文件', required=True, ext_whitelist=['.pdf'])

    class Meta:
        model = BoDuanFile
        fields = ['file_name']

    def save(self, commit=True):
        instance = super(BoDuanFileForm, self).save(commit=False)
        instance.file_name = instance.file_addr.name
        if commit:
            instance.save()
        return instance

class BoDuanFileInline(NestedStackedInline):
# class BoDuanFileInline(NestedTabularInline):
    model = BoDuanFile
    extra = 1
    fk_name = 'bo_duan'
    # readonly_fields = ('created_time',)
    exclude = ('file_name', 'created_time')

    form = BoDuanFileForm


class BoDuanInline(NestedStackedInline):
# class BoDuanInline(NestedTabularInline):
    model = BoDuan
    extra = 1
    fk_name = 'training'
    inlines = [BoDuanFileInline]
    exclude = ('created_time', )
    # readonly_fields = ('created_time',)

    list_display = ('bo_duan_name',)

class TrainingV1Admin(NestedModelAdmin):
    model = TrainingV1
    inlines = [BoDuanInline]
    fields = ('name', )
    # readonly_fields = ('created_time',)

    list_display = ('name',)

    def save_model(self, request, obj, form, change):
        # import pdb; pdb.set_trace()
        if not change:
            obj.user = request.user
        obj.save()

    '''
        override the css in admin
    '''
    class Media:
        # js = ('js/admin/my_own_admin.js',)    
        css = {
             'all': ('css/admin/my_own_admin.css',)
        }

admin.site.register(TrainingV1, TrainingV1Admin)